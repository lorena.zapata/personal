select * from (with inv as (select 
			date(fk_inventory_date) ,
			dcs.bk_car_stock,
			cast(dcs.sk_car_stock as varchar)||date(fk_inventory_date) as id,
            dcs.sk_car_stock,
			amt_olp_published_price,
			amt_olp_published_promotion_price,
			amt_olp_published_price - amt_olp_published_promotion_price as applied_discount,
			fi.bk_sku,
			case when dl.hub_name in ('Kavak Artz Pedregal S5') then 'Kavak Moliere' 
     		when dl.hub_name in ('Kavak Constituyentes','Kavak Hub Kaizen','Anexo Antara') then 'Kavak Moliere' else dl.hub_name end as hub_name,
			dis.inventory_status_desc
			from dwh.fact_inventory fi
			left join dwh.dim_location dl on fi.fk_olp_hub_location = dl.sk_location 
			left join dwh.dim_car_stock dcs on fi.fk_car_stock = dcs.sk_car_stock 
			left join dwh.dim_inventory_status dis on dis.sk_inventory_status = fi.fk_olp_car_inventory_status 
			where country_iso = 'MX'
			and fk_inventory_date > 20150101
			--and amt_olp_published_promotion_price is not null
			and flg_nts_stock is true
            --and dl.hub_name is not null
            --and dl.hub_name <>''            
			--and dcs.bk_car_stock = 172784 
			order by date(fk_inventory_date) desc),
inv2 as (select 
            date(fk_inventory_date) ,
			dcs.bk_car_stock,
			cast(dcs.sk_car_stock as varchar)||date(fk_inventory_date) as id,
            dcs.sk_car_stock,
			amt_olp_published_price,
			amt_olp_published_promotion_price,
			amt_olp_published_price - amt_olp_published_promotion_price as applied_discount,
			fi.bk_sku,
			case when dl.hub_name in ('Kavak Artz Pedregal S5') then 'Kavak Moliere' 
     		when dl.hub_name in ('Kavak Constituyentes','Kavak Hub Kaizen','Anexo Antara') then 'Kavak Moliere' else dl.hub_name end as hub_name,
			dis.inventory_status_desc
			from dwh.fact_inventory fi
			left join dwh.dim_location dl on fi.fk_olp_hub_location = dl.sk_location 
			left join dwh.dim_car_stock dcs on fi.fk_car_stock = dcs.sk_car_stock 
			left join dwh.dim_inventory_status dis on dis.sk_inventory_status = fi.fk_olp_car_inventory_status 
			where country_iso = 'MX'
			and fk_inventory_date > 20150101
			--and amt_olp_published_promotion_price is not null
			and flg_nts_stock is true
            and dl.hub_name is not null
            and dl.hub_name <>''            
			--and dcs.bk_car_stock = 172784 
			order by date(fk_inventory_date) desc),
master_sku_unicas as (select 
    distinct --extract (month from date(dd."date"))
    date(dd."date") as first_booking_date
    ,date(fb.fk_declared_sale_date ) as sale_date
	,date(fb.fk_sale_cancellation_date) as cancellation_date
	,date(fb.fk_delivery_date) as delivery_date
    ,dpt."type" as payment_type
    ,fb.bk_booking
    ,dc2.full_name
    ,case when dc.place_name in('Gdl - Midtown') then 'MIDTOWN' 
	        when dc.place_name in('Plaza Fortuna') then 'FORTUNA'  
	        when dc.place_name in('Lerma') then 'LERMA'
	        when dc.place_name in('Edomex - Cosmopol') then 'COSMOPOL'
	        when dc.place_name in('Qro - Puerta La Victoria') then 'PUERTA LA VICTORIA'
	        when dc.place_name in('Mty - Punto Valle') then 'PUNTO VALLE'
	        when dc.place_name in('Pue - Las Torres') then 'LAS TORRES'
	        when dc.place_name in('San Angel') then 'SAN ANGEL'
	        when dc.place_name in('Pue - Explanada') then 'EXPLANADA'
	        when dc.place_name in('Cdmx - El Rosario Town Center') then 'ROSARIO'
	        when dc.place_name in('Kavak Forum Cuernavaca') then 'CUERNAVACA'
	        when dc.place_name in('Cdmx - Artz Pedregal') then 'ARTZ'
	        when dc.place_name in('Patio Santa Fe') then 'SANTA FE'
	        when dc.place_name in('Estado De Mexico - Tlalnepantla') then 'TLALNEPANTLA'
	        when dc.place_name in('Gdl - Punto Sur') then 'PUNTO SUR'
	        when dc.place_name in('Cdmx - Antara Fashion Hall') then 'ANTARA'
	        when dc.place_name in('Mty - Fashion Drive') then 'FASHION DRIVE'
	        when dc.place_name in('Patio Tlalpan') then 'TLALPAN'
	        when dc.place_name in('Estado De Mexico - Interlomas') then 'INTERLOMAS'
	        when dc.place_name in('Mty - Nuevo Sur') then 'NUEVO SUR'
	        when dc.place_name in('Florencia') then 'FLORENCIA'
	        when dc.place_name in('Cdmx - Portal Centro') then 'PORTAL CENTRO'
	        when dc.place_name in('Cdmx - Las Antenas') then 'LAS ANTENAS'
	        when dc.place_name in('Qro - Paseo Queretaro') then 'PASEO'
	        when dc.place_name in('Pue - Parque Puebla') then 'PARQUE'
	        when dc.place_name in('Qro - Antea') then 'ANTEA'
	        when dc.place_name in('Patio Toluca') then 'PATIO TOLUCA'
	        when dc.place_name in('Cdmx - El Rosario Town Center') then 'ROSARIO'
	        when dc.place_name in('Gdl - Punto Sur') then 'PUNTO SUR'
	        when dc.place_name in('Gdl - Midtown') then 'MIDTOWN'
	        when dc.place_name in('Mty - Fashion Drive') then 'FASHION DRIVE'
	        when dc.place_name in('Pue - Explanada') then 'EXPLANADA'
	        when dc.place_name in('Pue - Las Torres') then 'LAS TORRES'
	        when dc.place_name in('Patio Santa Fe') then 'SANTA FE'
	        when dc.place_name in('San Angel') then 'SAN ANGEL'
	        when dc.place_name in('Patio Toluca') then 'PATIO TOLUCA'
	        when dc.place_name in('Qro - Puerta La Victoria') then 'PUERTA LA VICTORIA'
	        when dc.place_name in('Patio Santa Fe') then 'SANTA FE' else 'TELESALES' end as place_name
    --,dc2.bk_client 
    ,dc2.email
    ,dc2.phone--,ent.destination_name_end bk_sku
    ,case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.amt_olp_published_price end) is null then 
     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.amt_olp_published_price end) else
     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.amt_olp_published_price end) end as published_price
    ,case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.bk_sku end) is null then 
     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.bk_sku end) else
     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.bk_sku end) end as bk_sku
    ,case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.bk_car_stock end) is null then 
     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.bk_car_stock end) else
     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.bk_car_stock end) end as stock 
    ,case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end as location_found
,case WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Plaza Fortuna') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Lerma') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Patio Santa Fe') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Explanada') THEN 'PUE'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Punto Sur') THEN 'GDL'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Artz Pedregal') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak WH Lerma') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Patio Toluca') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Las Torres') THEN 'PUE'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Patio Tlalpan') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Forum Cuernavaca') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Midtown Guadalajara') THEN 'GDL'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Puerta la Victoria') THEN 'QRO'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Antara Fashion Hall') THEN 'GDL'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak El Rosario Town Center') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Portal San Ángel') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Tlaquepaque') THEN 'GDL'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('HQ Fashion Drive') THEN 'MTY'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Punto Valle') THEN 'MTY'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('HQ Explanada') THEN 'PUE'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Cuautlancingo') THEN 'PUE'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Paseo Querétaro') THEN 'QRO'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak City Qro') THEN 'QRO'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Fashion Drive') THEN 'MTY'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Cosmopol') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Techparc') THEN 'GDL'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Nuevo Sur') THEN 'MTY'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Antea') THEN 'QRO'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Tlalnepantla') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Florencia') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Angelópolis') THEN 'PUE'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Moliere') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Parque Puebla') THEN 'PUE'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Paseo Querétaro') THEN 'QRO'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Interlomas') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Kavak Las Antenas') THEN 'CDMX'
		WHEN (case when (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) is null then 
		     (case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end) else
		     (case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end) end) IN ('Mi domicilio / oficina') THEN '' else '' end as region_location         		     
	       ,case when date(dd."date") < '2022-06-01' then (case when (case when case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end in ('Kavak Patio Tlalpan'
	                            ,'Kavak Punto Valle'
	                            ,'Kavak Forum Cuernavaca'
	                            ,'Kavak Las Torres'
	                            ,'Kavak Midtown Guadalajara'
	                            ,'Kavak Puerta la Victoria'
	                            ,'Kavak Patio Santa Fe'
	                            ,'Kavak Cosmopol','Kavak Portal San Ángel'
	                            ,'Kavak Lerma'
	                            ,'Kavak Artz Pedregal'
	                            ,'Kavak Plaza Fortuna'
	                            ,'Kavak El Rosario Town Center'
	                            ,'Kavak Antara Fashion Hall'
	                            ,'Kavak Punto Sur'
	                            ,'Kavak Patio Toluca'
	                            ,'Kavak Explanada'
	                            ,'Kavak Nuevo Sur'
	                            ,'Kavak Fashion Drive'
	                            ,'Kavak Portal Centro'
	                            ,'Kavak Tlalnepantla'
	                            ,'Kavak Patio Tlalpan'
	                            ,'Kavak Florencia'
	                            ,'Kavak Antea'
	                            ,'Kavak Parque Puebla'
	                            ,'Kavak Paseo Querétaro'
	                            ,'Kavak Interlomas'
	                            ,'Kavak Las Antenas'
	                            ) then 'COMERCIAL'
	      when case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end in ('HQ Explanada'
	                            ,'Kavak Tlaquepaque'	                         
	                            ,'Kavak City Qro'
	                            ,'Kavak WH Lerma'
	                            ,'Kavak Techparc'
	                            ,'HQ Fashion Drive'
	                            ,'Kavak Cuautlancingo'
	                            ,'Kavak Vallejo'
	                            ,'Kavak Moliere'
	                            ,'Kavak Angelópolis'
	                            ,'Garantias Kavak Antara Postventa'
	                            ,'Kavak Xola'
	                            ,'Kavak Mixcoac') then 'STORAGE'
	       else 'S/LOCATION' end) = 'S/LOCATION' then (case when case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end in ('Kavak Patio Tlalpan'
																																					                            ,'Kavak Punto Valle'
																																					                            ,'Kavak Forum Cuernavaca'
																																					                            ,'Kavak Las Torres'
																																					                            ,'Kavak Midtown Guadalajara'
																																					                            ,'Kavak Puerta la Victoria'
																																					                            ,'Kavak Patio Santa Fe'
																																					                            ,'Kavak Cosmopol','Kavak Portal San Ángel'
																																					                            ,'Kavak Lerma'
																																					                            ,'Kavak Artz Pedregal'
																																					                            ,'Kavak Plaza Fortuna'
																																					                            ,'Kavak El Rosario Town Center'
																																					                            ,'Kavak Antara Fashion Hall'
																																					                            ,'Kavak Punto Sur'
																																					                            ,'Kavak Nuevo Sur'
																																					                            ,'Kavak Patio Toluca'
																																					                            ,'Kavak Explanada'
																																					                            ,'Kavak Fashion Drive'
																																					                            ,'Kavak Portal Centro'
																																					                            ,'Kavak Tlalnepantla'
																																					                            ,'Kavak Florencia'
																																					                            ,'Kavak Antea'
																																					                            ,'Kavak Parque Puebla'
																																					                            ,'Kavak Paseo Querétaro'
																																					                            ,'Kavak Interlomas'
																																					                            ,'Kavak Las Antenas') then 'COMERCIAL'
	     													 when case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end in ('HQ Explanada'
																																						                            ,'Kavak Tlaquepaque'	                         
																																						                            ,'Kavak City Qro'
																																						                            ,'Kavak WH Lerma'
																																						                            ,'Kavak Techparc'
																																						                            ,'HQ Fashion Drive'
																																						                            ,'Kavak Cuautlancingo'
																																						                            ,'Kavak Vallejo'
																																						                            ,'Kavak Moliere'
																																						                            ,'Kavak Angelópolis'
																																						                            ,'Garantias Kavak Antara Postventa'
																																						                            ,'Kavak Xola'
																																						                            ,'Kavak Mixcoac') then 'STORAGE'
																														       																else 'S/LOCATION' end) else (case when case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end in ('Kavak Patio Tlalpan'
																																																																														                            ,'Kavak Punto Valle'
																																																																														                            ,'Kavak Forum Cuernavaca'
																																																																														                            ,'Kavak Las Torres'
																																																																														                            ,'Kavak Midtown Guadalajara'
																																																																														                            ,'Kavak Puerta la Victoria'
																																																																														                            ,'Kavak Patio Santa Fe'
																																																																														                            ,'Kavak Cosmopol','Kavak Portal San Ángel'
																																																																														                            ,'Kavak Lerma'
																																																																														                            ,'Kavak Artz Pedregal'
																																																																														                            ,'Kavak Plaza Fortuna'
																																																																														                            ,'Kavak El Rosario Town Center'
																																																																														                            ,'Kavak Antara Fashion Hall'
																																																																														                            ,'Kavak Punto Sur'
																																																																														                            ,'Kavak Nuevo Sur'
																																																																														                            ,'Kavak Patio Toluca'
																																																																														                            ,'Kavak Explanada'
																																																																														                            ,'Kavak Fashion Drive'
																																																																														                            ,'Kavak Portal Centro'
																																																																														                            ,'Kavak Tlalnepantla'
																																																																														                            ,'Kavak Florencia'
																																																																														                            ,'Kavak Antea'
																																																																														                            ,'Kavak Parque Puebla'
																																																																														                            ,'Kavak Paseo Querétaro'
																																																																														                            ,'Kavak Interlomas'
																																																																														                            ,'Kavak Las Antenas') then 'COMERCIAL'
																																																																															      when case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end in ('HQ Explanada'
																																																																																																											                            ,'Kavak Tlaquepaque'	                         
																																																																																																											                            ,'Kavak City Qro'
																																																																																																											                            ,'Kavak WH Lerma'
																																																																																																											                            ,'Kavak Techparc'
																																																																																																											                            ,'HQ Fashion Drive'
																																																																																																											                            ,'Kavak Cuautlancingo'
																																																																																																											                            ,'Kavak Vallejo'
																																																																																																											                            ,'Kavak Moliere'
																																																																																																											                            ,'Kavak Angelópolis'
																																																																																																											                            ,'Garantias Kavak Antara Postventa'
																																																																																																											                            ,'Kavak Xola'
																																																																																																											                            ,'Kavak Mixcoac') then 'STORAGE' else 'S/LOCATION' end) end) else  
(case when (case when case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end in ('Kavak Patio Tlalpan'
	                            ,'Kavak Punto Valle'
	                            ,'Kavak Forum Cuernavaca'
	                            ,'Kavak Las Torres'
	                            ,'Kavak Midtown Guadalajara'
	                            ,'Kavak Puerta la Victoria'
	                            ,'Kavak Patio Santa Fe'
	                            ,'Kavak Cosmopol','Kavak Portal San Ángel'
	                            ,'Kavak Lerma'
	                            ,'Kavak Artz Pedregal'
	                            ,'Kavak Plaza Fortuna'
	                            ,'Kavak El Rosario Town Center'
	                            ,'Kavak Antara Fashion Hall'
	                            ,'Kavak Punto Sur'
	                            ) then 'COMERCIAL'
	      when case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end in ('HQ Explanada'
	                            ,'Kavak Tlaquepaque'
	                            ,'Kavak Patio Toluca'
	                            ,'Kavak City Qro'
	                            ,'Kavak WH Lerma'
	                            ,'Kavak Techparc'
	                            ,'Kavak Explanada'
	                            ,'Kavak Fashion Drive'
	                            ,'HQ Fashion Drive'
	                            ,'Kavak Cuautlancingo'
	                            ,'Kavak Portal Centro'
	                            ,'Kavak Tlalnepantla'
	                            ,'Kavak Vallejo'
	                            ,'Kavak Florencia'
	                            ,'Kavak Nuevo Sur'
	                            ,'Kavak Antea'
	                            ,'Kavak Moliere'
	                            ,'Kavak Angelópolis'
	                            ,'Kavak Parque Puebla'
	                            ,'Garantias Kavak Antara Postventa'
	                            ,'Kavak Paseo Querétaro'
	                            ,'Kavak Interlomas'
	                            ,'Kavak Las Antenas'
	                            ,'Kavak Xola'
	                            ,'Kavak Mixcoac') then 'STORAGE'
	       else 'S/LOCATION' end) = 'S/LOCATION' then (case when case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end in ('Kavak Patio Tlalpan'
										                            ,'Kavak Punto Valle'
										                            ,'Kavak Forum Cuernavaca'
										                            ,'Kavak Las Torres'
										                            ,'Kavak Midtown Guadalajara'
										                            ,'Kavak Puerta la Victoria'
										                            ,'Kavak Patio Santa Fe'
										                            ,'Kavak Cosmopol','Kavak Portal San Ángel'
										                            ,'Kavak Lerma'
										                            ,'Kavak Artz Pedregal'
										                            ,'Kavak Plaza Fortuna'
										                            ,'Kavak El Rosario Town Center'
										                            ,'Kavak Antara Fashion Hall'
										                            ,'Kavak Punto Sur'
										                            ) then 'COMERCIAL'
	     													 when case when date(fb.fk_declared_sale_date)-1 = inv2.date and inv2.hub_name is not null then inv2.hub_name end in ('HQ Explanada'
																																							                            ,'Kavak Tlaquepaque'
																																							                            ,'Kavak Patio Toluca'
																																							                            ,'Kavak City Qro'
																																							                            ,'Kavak WH Lerma'
																																							                            ,'Kavak Techparc'
																																							                            ,'Kavak Explanada'
																																							                            ,'Kavak Fashion Drive'
																																							                            ,'HQ Fashion Drive'
																																							                            ,'Kavak Cuautlancingo'
																																							                            ,'Kavak Portal Centro'
																																							                            ,'Kavak Tlalnepantla'
																																							                            ,'Kavak Vallejo'
																																							                            ,'Kavak Florencia'
																																							                            ,'Kavak Antea'
																																							                            ,'Kavak Moliere'
																																							                            ,'Kavak Nuevo Sur'
																																							                            ,'Kavak Angelópolis'
																																							                            ,'Kavak Parque Puebla'
																																							                            ,'Garantias Kavak Antara Postventa'
																																							                            ,'Kavak Paseo Querétaro'
																																							                            ,'Kavak Interlomas'
																																							                            ,'Kavak Las Antenas'
																																							                            ,'Kavak Xola'
																																							                            ,'Kavak Mixcoac') then 'STORAGE'
																														       																else 'S/LOCATION' end) else (case when case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end in ('Kavak Patio Tlalpan'
																																																																															                            ,'Kavak Punto Valle'
																																																																															                            ,'Kavak Forum Cuernavaca'
																																																																															                            ,'Kavak Las Torres'
																																																																															                            ,'Kavak Midtown Guadalajara'
																																																																															                            ,'Kavak Puerta la Victoria'
																																																																															                            ,'Kavak Patio Santa Fe'
																																																																															                            ,'Kavak Cosmopol','Kavak Portal San Ángel'
																																																																															                            ,'Kavak Lerma'
																																																																															                            ,'Kavak Artz Pedregal'
																																																																															                            ,'Kavak Plaza Fortuna'
																																																																															                            ,'Kavak El Rosario Town Center'
																																																																															                            ,'Kavak Antara Fashion Hall'
																																																																															                            ,'Kavak Punto Sur'
																																																																															                            ) then 'COMERCIAL'
																																																																															      when case when date(fb.fk_booking_creation_date) = inv.date and inv.hub_name is not null then inv.hub_name end in ('HQ Explanada'
																																																																															                            ,'Kavak Tlaquepaque'
																																																																															                            ,'Kavak Patio Toluca'
																																																																															                            ,'Kavak City Qro'
																																																																															                            ,'Kavak WH Lerma'
																																																																															                            ,'Kavak Techparc'
																																																																															                            ,'Kavak Explanada'
																																																																															                            ,'Kavak Fashion Drive'
																																																																															                            ,'HQ Fashion Drive'
																																																																															                            ,'Kavak Cuautlancingo'
																																																																															                            ,'Kavak Portal Centro'
																																																																															                            ,'Kavak Tlalnepantla'
																																																																															                            ,'Kavak Vallejo'
																																																																															                            ,'Kavak Florencia'
																																																																															                            ,'Kavak Antea'
																																																																															                            ,'Kavak Moliere'
																																																																															                            ,'Kavak Nuevo Sur'
																																																																															                            ,'Kavak Angelópolis'
																																																																															                            ,'Kavak Parque Puebla'
																																																																															                            ,'Garantias Kavak Antara Postventa'
																																																																															                            ,'Kavak Paseo Querétaro'
																																																																															                            ,'Kavak Interlomas'
																																																																															                            ,'Kavak Las Antenas'
																																																																															                            ,'Kavak Xola'
																																																																															                            ,'Kavak Mixcoac') then 'STORAGE'
																																																																															       else 'S/LOCATION' end) end) end as location_del_auto_en_la_reserva            
from dwh.fact_booking fb left join dwh.dim_date dd on dd.sk_date =fb.fk_first_booking_creation_date  
             left join dwh.dim_payment_type dpt on dpt.sk_payment_type =fb.fk_payment_type 
             LEFT JOIN dwh.dim_geography dg ON dg.sk_geography =fb.fk_client_geo 
             left join dwh.dim_channel dc on dc.sk_channel = fb.fk_first_booking_channel_location 
             left join dwh.dim_client dc2 on dc2.sk_client=fb.fk_client 
             left join dwh.dim_booking_type dbt on dbt.sk_booking_type =fb.fk_booking_type 
             left join dwh.dim_location dl on dl.sk_location = fb.fk_car_booking_location 
             left join dwh.dim_employee de on de.sk_employee = fb.fk_booked_by 
             left join inv on inv.id = cast(fb.fk_stock_id as varchar)||date(fb.fk_booking_creation_date)
             left join inv2 on inv2.id = cast(fb.fk_stock_id as varchar)||date(fb.fk_booking_creation_date)-1
             left join (select sd.country_item
							,fb.bk_booking
							,sd.origin_delivery_id
							,ori.destination_name as origin_name
							,sd.destination_delivery_id
							,case when des.destination_name is null or des.destination_name='' then 'S/ENTREGA_AGENDADA' else des.destination_name end as destination_name_end
							from stg.sales_delivery sd left join dwh.fact_booking fb on fb.bk_booking=sd.bk_booking
							left join dwh.dim_client dc on fb.fk_client =dc.sk_client
							left join stg.destination_delivery des on des.bk_destination =sd.destination_delivery_id
							left join stg.destination_delivery ori on ori.bk_destination = sd.origin_delivery_id
							where sd.country_item ='MX'
							/*and des.destination_name is not null*/) ent on ent.bk_booking = fb.bk_booking 
             --join t on fb.bk_booking = t.trn_id
where date(dd."date")>= '2022-01-01'
and fb.flag_main_booking=true
and dg.country_iso='MX'
/*and dc.place_name in ('San Angel',
						'Qro - Puerta La Victoria',
						'Qro - Paseo Queretaro',
						'Qro - Antea',
						'Pue - Parque Puebla',
						'Pue - Las Torres',
						'Pue - Explanada',
						'Plaza Fortuna',
						'Patio Tlalpan',
						'Patio Santa Fe',
						'Mty - Punto Valle',
						'Mty - Nuevo Sur',
						'Mty - Fashion Drive',
						'Lerma',
						'Kavak Forum Cuernavaca',
						'Gdl - Punto Sur',
						'Gdl - Midtown',
						'Florencia',
						'Estado De Mexico - Tlalnepantla',
						'Estado De Mexico - Interlomas',
						'Edomex - Cosmopol',
						'Cdmx - Portal Centro',
						'Cdmx - Las Antenas',
						'Cdmx - El Rosario Town Center',
						'Cdmx - Artz Pedregal',
						'Cdmx - Antara Fashion Hall',
						'Edomex - Patio Toluca'
						)*/
--group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
order by 1 desc)
select 
first_booking_date
,case when location_del_auto_en_la_reserva = 'STORAGE' then 'ONLINE'
      when location_del_auto_en_la_reserva = 'COMERCIAL' then 'STOCK ID' else 'S/LOCATION' end as ONLINE_StockID   
,place_name
,payment_type
,bk_booking
,stock
,bk_sku
,location_found
,published_price
from master_sku_unicas msu
order by 1 desc) limit 200000