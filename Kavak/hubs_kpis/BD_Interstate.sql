select  ls.logistic_service_id, ls.service_date, ls.car_stock_id, ls.service_origin_hub_name, ls.service_origin_region_name,  ls.service_destiny_hub_name, ls.service_destiny_region_name, ls.service_type_name, ls.service_status_name ,
date_trunc('day',ls.started_date) as started_date,  date_trunc('day',ls.finished_date) as finished_date,
ls.financial_category
from serving.logistic_service ls 
where interstate_movement_flg = true and service_status_name in ('Closed','Finished') and ls.service_date >= '2022-01-01' and ls.service_date <= '2023-06-30' and ls.country_iso = 'MX'
