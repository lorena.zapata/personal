---maintenance penetration
select 
	dc.bk_client as client_id,
	count(fmp.bk_maintenance) as count_mtce
from dwh.fact_booking fb 
left join dwh.dim_car_stock dcs on dcs.sk_car_stock = fb.fk_stock_id 
left join dwh.dim_client dc on dc.sk_client = fb.fk_client 
left join dwh.fact_maintenance_procedure fmp on fmp.bk_estimate = fb.bk_booking and fmp.bk_stock_id = dcs.bk_car_stock 
group by 1
order by 2 desc
limit 100;
--maintenance by stock
select 
	fmp.bk_stock_id,
	count(distinct fmp.bk_maintenance) as mtces
from dwh.fact_maintenance_procedure fmp 
left join dwh.dim_maintenance_status dms on dms.sk_maintenance_status = fmp.fk_current_status 
where dms.bk_maintenance_status in (18, 19, 20, 21, 22, 23) --status Delivered, Parts Received, in-warehouse, service finished, tobedelivered, in-service
and fmp.bk_stock_id is not null
group by 1
order by 2 desc;
