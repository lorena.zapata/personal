with inv_changes as(
 select dd."date" as date,
 dcs.bk_car_stock,
case when dl.hub_name is null then 'No Location' else dl.hub_name end as current_location,
  lag (dcs.bk_car_stock) over (order by dcs.bk_car_stock , dd."date") as prev_stock,
 lag (current_location) over (order by dcs.bk_car_stock , dd."date") as prev_location ,
 case when dcs.bk_car_stock <> prev_stock then 1
  when current_location <> prev_location then 1 when current_location is null then 0 when prev_location is null then 1 else 0 end as change_location_flag
from dwh.fact_inventory fi
left join dwh.dim_date dd  on fi.fk_inventory_date = dd.sk_date
left join dwh.dim_date dd2  on fi.fk_nts_item_receipt_date  = dd2.sk_date
left join dwh.dim_date dd3  on fi.fk_olp_published_date = dd3.sk_date
left join dwh.dim_car_stock dcs on fi.fk_car_stock = dcs.sk_car_stock
left join dwh.dim_car_sku dcs2 on fi.fk_car_sku  = dcs2.sk_car_sku
left join dwh.dim_inventory_status dis on fi.fk_olp_car_inventory_status = dis.sk_inventory_status
left join dwh.dim_location dl on fi.fk_olp_hub_location = dl.sk_location
where fi.country_iso ='MX' and current_location not in ('Mi domicilio / oficina','No Location')
and dd."date" >= '2022-01-01' and dd."date" <= '2023-06-30'
and dis.bk_inventory_status IN (1,2)
and (fi.flg_olp_stock  = 1)
)
select *
from inv_changes
where change_location_flag= 1
order by 2,1