with 
dates as (select 20230101 as date_parameter )
,inv1 as ( -- todas las ubicaciones
		select
			date(fk_inventory_date) ,
			dcs.bk_car_stock,
			cast(dcs.sk_car_stock as varchar)||date(fk_inventory_date) as stock_fecha,
            dcs.sk_car_stock,
			--amt_olp_published_price,
			--amt_olp_published_promotion_price,
			--amt_olp_published_price - amt_olp_published_promotion_price as applied_discount,
			dl.hub_name,
			dis.inventory_status_desc
			from dwh.fact_inventory fi
			left join dwh.dim_location dl on fi.fk_olp_hub_location = dl.sk_location
			left join dwh.dim_car_stock dcs on fi.fk_car_stock = dcs.sk_car_stock
			left join dwh.dim_inventory_status dis on dis.sk_inventory_status = fi.fk_olp_car_inventory_status
			left join dates on 1=1
			where country_iso = 'MX'
			and fk_inventory_date > date_parameter
			and flg_nts_stock is true
			--order by date(fk_inventory_date) desc
			)
,inv2 as ( -- solohubs
			select
            date(fk_inventory_date) ,
			dcs.bk_car_stock,
			cast(dcs.sk_car_stock as varchar)||date(fk_inventory_date) as stock_fecha,
            dcs.sk_car_stock,
			--amt_olp_published_price,
			--amt_olp_published_promotion_price,
			--amt_olp_published_price - amt_olp_published_promotion_price as applied_discount,
			dl.hub_name,
			dis.inventory_status_desc
			from dwh.fact_inventory fi
			left join dwh.dim_location dl on fi.fk_olp_hub_location = dl.sk_location
			left join dwh.dim_car_stock dcs on fi.fk_car_stock = dcs.sk_car_stock
			left join dwh.dim_inventory_status dis on dis.sk_inventory_status = fi.fk_olp_car_inventory_status
			left join dates on 1=1
			where country_iso = 'MX'
			and fk_inventory_date > date_parameter
			and flg_nts_stock is true
            and dl.hub_name is not null
            and dl.hub_name <>''
            --limit 10
			--order by date(fk_inventory_date) desc
			)
,inventory as (
		select distinct 
		fb.bk_booking as estimate
			  ,st.bk_car_stock as Stock
			  ,fb.fk_client as fk_client
			  ,dc.bk_client as bk_client
			  ,dc.bk_client_olimpo as olimpo_id
			  ,dc.email as email
			  ,st.bk_car_stock
			  ,st.bk_item 
			  ,st.sku 
			  ,date(fb.fk_booking_creation_date) as booking_creation_date
			  ,date(fb.fk_booking_cancellation_date) as booking_cancellation_date
			  ,date(fb.fk_delivery_date) as delivery_date
			  ,date(fk_credit_memo_creation_date) as return_date
			  ,case when flag_gross_delivered IS TRUE then 1 else 0 end flag_delivery
			  ,case when flag_returned IS TRUE then 1 else 0 end flag_return
			  ,amt_car_publication_price
			  ,amt_campaign_discount
			  ,amt_total_discount
			  ,amt_sales_invoice
			  ,amt_sales_tax
			  ,amt_booking_payment
			  ,amt_first_publication_sale_price
			  ,fb.amt_sales_invoice - fb.amt_sales_tax as booking_net_sales_price
			  ,inv1.hub_name
			  ,inv2.hub_name as hub_name_2
		      ,dl.location_name as location_booking
		from dwh.fact_booking fb
		left join dwh.dim_geography dg on fb.fk_client_geo = dg.sk_geography 
		left join dwh.dim_date dd1 on fb.fk_booking_creation_date = dd1.sk_date 
		left join dwh.dim_client dc on fb.fk_client = dc.sk_client
		left join dwh.dim_car_stock st on fb.fk_stock_id = st.sk_car_stock 
		/*
		left join dwh.dim_date dd2 on fb.fk_booking_cancellation_date = dd2.sk_date 
		left join dwh.dim_date dd3 on fb.fk_declared_sale_date = dd3.sk_date
		left join dwh.dim_date dd4 on fb.fk_booking_cancellation_date = dd4.sk_date 
		left join dwh.dim_date dd5 on fb.fk_first_booking_creation_date = dd5.sk_date
		left join dwh.dim_date dd12 on fb.fk_sale_cancellation_date = dd12.sk_date
		*/
		left join inv1 on inv1.stock_fecha = cast(st.sk_car_stock as varchar)||date(fb.fk_booking_creation_date)
		left join inv2 on inv2.stock_fecha = cast(st.sk_car_stock as varchar)||date(fb.fk_booking_creation_date)-1
		left join dwh.dim_location dl on dl.sk_location = fb.fk_car_booking_location
		left join dates on 1=1
		where dg.country_iso = 'MX'
		and dd1."date" between date(date_parameter) and current_date-1
		and flag_gross_delivered IS true and fb.fk_delivery_date is not null
		)	
,region as (
	select *,  case when hub_name = '' then 'NULL'
            when hub_name is null then 'NULL'
            when hub_name = 'Garantias Kavak Antara Postventa' THEN 'CDMX'
            when hub_name = 'HQ Explanada' THEN 'PUE'
            when hub_name = 'HQ Fashion Drive' THEN 'MTY'
            when hub_name = 'Kavak Angelópolis' THEN 'NULL'
            when hub_name = 'Kavak Antara Fashion Hall' THEN 'CDMX'
            when hub_name = 'Kavak Antea' THEN 'QRO'
            when hub_name = 'Kavak Artz Pedregal' THEN 'CDMX'
            when hub_name = 'Kavak Artz Pedregal S5' THEN 'CDMX'
            when hub_name = 'Kavak City Qro' THEN 'QRO'
            when hub_name = 'Kavak Cosmopol' THEN 'CDMX'
            when hub_name = 'Kavak Cuautlancingo' THEN 'CDMX'
            when hub_name = 'Kavak El Rosario Town Center' THEN 'CDMX'
            when hub_name = 'Kavak Explanada' THEN 'CDMX'
            when hub_name = 'Kavak Fashion Drive' THEN 'MTY'
            when hub_name = 'Kavak Florencia' THEN 'CDMX'
            when hub_name = 'Kavak Forum Cuernavaca' THEN 'CDMX'
            when hub_name = 'Kavak Hub Kaizen' THEN 'CDMX'
            when hub_name = 'Kavak Interlomas' THEN 'CDMX'
            when hub_name = 'Kavak Las Antenas' THEN 'CDMX'
            when hub_name = 'Kavak Las Torres' THEN 'PUE'
            when hub_name = 'Kavak Lerma' THEN 'CDMX'
            when hub_name = 'Kavak Midtown Guadalajara' THEN 'GDL'
            when hub_name = 'Kavak Mixcoac' THEN 'CDMX'
            when hub_name = 'Kavak Moliere' THEN 'CDMX'
            when hub_name = 'Kavak Nuevo Sur' THEN 'MTY'
            when hub_name = 'Kavak Parque Puebla' THEN 'PUE'
            when hub_name = 'Kavak Paseo Querétaro' THEN 'QRO'
            when hub_name = 'Kavak Patio Santa Fe' THEN 'CDMX'
            when hub_name = 'Kavak Patio Tlalpan' THEN 'CDMX'
            when hub_name = 'Kavak Patio Tlalpan' THEN 'CDMX'
            when hub_name = 'Kavak Patio Toluca' THEN 'CDMX'
            when hub_name = 'Kavak Plaza Fortuna' THEN 'CDMX'
            when hub_name = 'Kavak Portal Centro' THEN 'CDMX'
            when hub_name = 'Kavak Portal San Ángel' THEN 'CDMX'
            when hub_name = 'Kavak Puerta la Victoria' THEN 'QRO'
            when hub_name = 'Kavak Punto Valle' THEN 'MTY'
            when hub_name = 'Kavak Techparc' THEN 'CDMX'
            when hub_name = 'Kavak Tlalnepantla' THEN 'CDMX'
            when hub_name = 'Kavak Tlaquepaque' THEN 'CDMX'
            when hub_name = 'Kavak Vallejo' THEN 'CDMX'
            when hub_name = 'Kavak WH Lerma' THEN 'CDMX'
            when hub_name = 'Kavak Xola' THEN 'CDMX'
            when hub_name ='Mi domicilio / oficina' THEN 'NULL'
            ELSE 'REVISAR'  end  region1,
        case when hub_name_2 = '' THEN 'NULL' 
            when hub_name_2 is null  THEN 'NULL'
            when hub_name_2 = 'Anexo Antara' THEN 'CDMX'
            when hub_name_2 = 'Garantias Kavak Antara Postventa' THEN 'CDMX'
            when hub_name_2 = 'HQ Explanada' THEN 'PUE'
            when hub_name_2 = 'HQ Fashion Drive' THEN 'MTY'
            when hub_name_2 = 'Kavak Angelópolis' THEN 'CDMX'
            when hub_name_2 = 'Kavak Antara Fashion Hall' THEN 'CDMX'
            when hub_name_2 = 'Kavak Antea' THEN 'QRO'
            when hub_name_2 = 'Kavak Artz Pedregal' THEN 'CDMX'
            when hub_name_2 = 'Kavak Artz Pedregal S5' THEN 'CDMX'
            when hub_name_2 = 'Kavak City Qro' THEN 'QRO'
            when hub_name_2 = 'Kavak Cosmopol' THEN 'CDMX'
            when hub_name_2 = 'Kavak Cuautlancingo' THEN 'CDMX'
            when hub_name_2 = 'Kavak El Rosario Town Center' THEN 'CDMX'
            when hub_name_2 = 'Kavak Explanada' THEN 'PUE'
            when hub_name_2 = 'Kavak Fashion Drive' THEN 'MTY'
            when hub_name_2 = 'Kavak Florencia' THEN 'CDMX'
            when hub_name_2 = 'Kavak Forum Cuernavaca' THEN 'CDMX'
            when hub_name_2 = 'Kavak Hub Kaizen' THEN 'CDMX'
            when hub_name_2 = 'Kavak Interlomas' THEN 'CDMX'
            when hub_name_2 = 'Kavak Las Antenas' THEN 'QRO'
            when hub_name_2 = 'Kavak Las Torres' THEN 'PUE'
            when hub_name_2 = 'Kavak Lerma' THEN 'CDMX'
            when hub_name_2 = 'Kavak Midtown Guadalajara' THEN 'GDL'
            when hub_name_2 = 'Kavak Mixcoac' THEN 'CDMX'
            when hub_name_2 = 'Kavak Moliere' THEN 'CDMX'
            when hub_name_2 = 'Kavak Nuevo Sur' THEN 'MTY'
            when hub_name_2 = 'Kavak Parque Puebla' THEN 'PUE'
            when hub_name_2 = 'Kavak Paseo Querétaro' THEN 'QRO'
            when hub_name_2 = 'Kavak Patio Santa Fe' THEN 'CDMX'
            when hub_name_2 = 'Kavak Patio Tlalpan' THEN 'CDMX'
            when hub_name_2 = 'Kavak Patio Toluca' THEN 'CDMX'
            when hub_name_2 = 'Kavak Plaza Fortuna' THEN 'CDMX'
            when hub_name_2 = 'Kavak Portal Centro' THEN 'CDMX'
            when hub_name_2 = 'Kavak Portal San Ángel' THEN 'CDMX'
            when hub_name_2 = 'Kavak Puerta la Victoria' THEN 'QRO'
            when hub_name_2 = 'Kavak Punto Sur' THEN 'GDL'
            when hub_name_2 = 'Kavak Punto Valle' THEN 'MTY'
            when hub_name_2 = 'Kavak Techparc' THEN 'CDMX'
            when hub_name_2 = 'Kavak Tlalnepantla' THEN 'CDMX'
            when hub_name_2 = 'Kavak Tlaquepaque' THEN 'CDMX'
            when hub_name_2 = 'Kavak Vallejo' THEN 'CDMX'
            when hub_name_2 = 'Kavak WH Lerma' THEN 'CDMX'
            when hub_name_2 = 'Kavak Xola' THEN 'CDMX'
            when hub_name_2 = 'Mi domicilio / oficina' THEN 'NULL'
            ELSE 'REVISAR' end region2,
        case when location_booking = '' then 'NULL'
            when location_booking is null THEN 'NULL'
            when location_booking = 'AGENCIA' THEN 'NULL'
            when location_booking = 'AGENCIA - GARANTIA' THEN 'NULL'
            when location_booking = 'ALL' THEN 'NULL'
            when location_booking = 'AUTO EN PRESTAMO A CLIENTE' THEN 'NULL'
            when location_booking = 'AUTOS ACTIVO FIJO' THEN 'NULL'
            when location_booking = 'CDI - VALLEJO' THEN 'CDMX'
            when location_booking = 'CDMX - ANTARA FASHION HALL' THEN 'CDMX'
            when location_booking = 'CDMX - ARTZ PEDREGAL' THEN 'CDMX'
            when location_booking = 'CDMX - EL ROSARIO TC - GARANTIA' THEN 'CDMX'
            when location_booking = 'CDMX - EL ROSARIO TOWN CENTER' THEN 'CDMX'
            when location_booking = 'CDMX - LAS ANTENAS' THEN 'CDMX'
            when location_booking = 'CDMX - OFC-CAPITAL REFORMA' THEN 'CDMX'
            when location_booking = 'CDMX - PORTAL CENTRO' THEN 'CDMX'
            when location_booking = 'DEVUELTO A VENDEDOR' THEN 'NULL'
            when location_booking = 'EDOMEX - COSMOPOL' THEN 'CDMX'
            when location_booking = 'EDOMEX - INTERLOMAS - GARANTIA' THEN 'CDMX'
            when location_booking = 'EDOMEX - PATIO TOLUCA' THEN 'CDMX'
            when location_booking = 'EDOMEX - TLALNEPANTLA - GARANTI' THEN 'CDMX'
            when location_booking = 'EN TRANSITO' THEN 'NULL'
            when location_booking = 'EN TRANSITO - GARANTIA' THEN 'NULL'
            when location_booking = 'EN TRANSITO - GDL' THEN 'NULL'
            when location_booking = 'EN TRANSITO - MORELOS' THEN 'NULL'
            when location_booking = 'EN TRANSITO - MORELOS GARANTIA' THEN 'NULL'
            when location_booking = 'EN TRANSITO - MTY' THEN 'NULL'
            when location_booking = 'EN TRANSITO - MTY GARANTIA' THEN 'NULL'
            when location_booking = 'EN TRANSITO - PUE' THEN 'NULL'
            when location_booking = 'EN TRANSITO - PUE GARANTIA' THEN 'NULL'
            when location_booking = 'EN TRANSITO - QRO' THEN 'NULL'
            when location_booking = 'EN TRANSITO - QRO GARANTIA' THEN 'NULL'
            when location_booking = 'ENTREGADO - 7 DIAS' THEN 'NULL'
            when location_booking = 'ENTREGADO - GARANTIA VIGENTE' THEN 'NULL'
            when location_booking = 'ESTADO DE MEXICO - INTERLOMAS' THEN 'CDMX'
            when location_booking = 'ESTADO DE MEXICO - TLALNEPANTLA' THEN 'CDMX'
            when location_booking = 'FINANZAS' THEN 'NULL'
            when location_booking = 'FLORENCIA' THEN 'CDMX'
            when location_booking = 'FLORENCIA - GARANTIA' THEN 'CDMX'
            when location_booking = 'GARANTIAS KAVAK ANTARA POSTVENTA' THEN 'CDMX'
            when location_booking = 'GDL - MIDTOWN' THEN 'GDL'
            when location_booking = 'GDL - PUNTO SUR' THEN 'GDL'
            when location_booking = 'HQ - GUADALAJARA' THEN 'GDL'
            when location_booking = 'HQ - GUADALAJARA GARANTIA' THEN 'GDL'
            when location_booking = 'HQ - LERMA' THEN 'CDMX'
            when location_booking = 'HQ - LERMA GARANTIA' THEN 'CDMX'
            when location_booking = 'HQ - MOLIERE' THEN 'CDMX'
            when location_booking = 'HQ - MOLIERE - GARANTIA' THEN 'CDMX'
            when location_booking = 'KAVAK ANGELOPOLIS' THEN 'NULL'
            when location_booking = 'KAVAK ARTZ PEDREGAL S5' THEN 'CDMX'
            when location_booking = 'KAVAK CUAUTLANCINGO' THEN 'NULL'
            when location_booking = 'KAVAK FORUM CUERNAVACA' THEN 'CDMX'
            when location_booking = 'KAVAK FORUM CUERNAVACA - GARANTIA' THEN 'CDMX'
            when location_booking = 'KAVAK HUB KAIZEN' THEN 'NULL'
            when location_booking = 'KAVAK TECHPARC' THEN 'NULL'
            when location_booking = 'MAIS SHOPPING' THEN 'NULL'
            when location_booking = 'MTY - FASHION DRIVE' THEN 'MTY'
            when location_booking = 'MTY - FASHION DRIVE - GARANTIA' THEN 'MTY'
            when location_booking = 'MTY - HQ FASHION DRIVE' THEN 'MTY'
            when location_booking = 'MTY - HQ FASHION DRIVE - GARANTIA' THEN 'MTY'
            when location_booking = 'MTY - NUEVO SUR' THEN 'MTY'
            when location_booking = 'MTY - PUNTO VALLE' THEN 'MTY'
            when location_booking = 'MXBA - WH - LERMA' THEN 'CDMX'
            when location_booking = 'PATIO TLALPAN' THEN 'CDMX'
            when location_booking = 'PATIO TLALPAN - GARANTIA' THEN 'CDMX'
            when location_booking = 'PICK UP CASA CLIENTE TRANSITO' THEN 'NULL'
            when location_booking = 'PLAZA FORTUNA' THEN 'CDMX'
            when location_booking = 'PLAZA FORTUNA - GARANTIA' THEN 'CDMX'
            when location_booking = 'PLAZA OUTLET LERMA' THEN 'CDMX'
            when location_booking = 'PUE - EXPLANADA' THEN 'PUE'
            when location_booking = 'PUE - EXPLANADA - GARANTIA' THEN 'PUE'
            when location_booking = 'PUE - HQ EXPLANADA' THEN 'PUE'
            when location_booking = 'PUE - HQ EXPLANADA - GARANTIA' THEN 'PUE'
            when location_booking = 'PUE - LAS TORRES' THEN 'PUE'
            when location_booking = 'PUE - PARQUE PUEBLA' THEN 'PUE'
            when location_booking = 'QRO - ANTEA' THEN 'QRO'
            when location_booking = 'QRO - KAVAK CITY' THEN 'QRO'
            when location_booking = 'QRO - KAVAK CITY - GARANTIA' THEN 'QRO'
            when location_booking = 'QRO - PASEO QUERETARO' THEN 'QRO'
            when location_booking = 'QRO - PUERTA LA VICTORIA' THEN 'QRO'
            when location_booking = 'QRO - PUERTA LA VICTORIA - GARA' THEN 'QRO'
            when location_booking = 'SAN ANGEL' THEN 'CDMX'
            when location_booking = 'SAN ANGEL - GARANTIA' THEN 'CDMX'
            when location_booking = 'SANTA FE' THEN 'CDMX'
            when location_booking = 'SANTA FE - GARANTIA' THEN 'CDMX'
            when location_booking = 'STOCK - CANGREJO' THEN 'NULL'
            when location_booking = 'STOCK - REPARACION HYP KAVAK' THEN 'NULL'
            when location_booking = 'STOCK - REPARACION HYP KV - MTY' THEN 'NULL'
            when location_booking = 'TOM' THEN 'NULL'
            when location_booking = 'VENDIDO' THEN 'NULL'
            when location_booking = 'VERIFICENTRO' THEN 'NULL'
            when location_booking = 'WH - LERMA 2' THEN 'CDMX'
            when location_booking = 'WH - LERMA 3' THEN 'CDMX'
            when location_booking = 'WH - LERMA 3 GARANTIA' THEN 'CDMX'
            when location_booking = 'WH - LERMA 4' THEN 'CDMX'
            when location_booking = 'WH - LERMA 4 GARANTIA' THEN 'CDMX'
            when location_booking = 'WH - LERMA 5' THEN 'CDMX'
            when location_booking = 'WH - LERMA 5 GARANTIA' THEN 'CDMX'
            when location_booking = 'WH - LERMA 6' THEN 'CDMX'
            when location_booking = 'WH - LERMA - GARANTIA' THEN 'CDMX'
            when location_booking = 'WH - LERMA ANEXO' THEN 'CDMX'
            when location_booking = 'WH - LERMA ANEXO GARANTIA' THEN 'CDMX'
            end region3
    from inventory)
    
select *
		--,to_char(delivery_date,'YYYY-MM') as "period",count(*) gross_delivery,sum(nvl2(return_date,1,0)) "returns", gross_delivery-"returns"
       ,case when region1 = 'NULL' and region2 = 'NULL' then region3 when region1 = 'NULL' then region2 else region1 end as region
    from region
);
    
/*
 * 
 * CRUCE CON BASE DE CLIENTES
 * 
 * */

with
clientes as (
select b.*,
coalesce(uh.hub,uh2.hub,uh3.hub) customer_hub
--,coalesce(uh.hub,uh2.hub,uh3.hub) customer_hub

--delivery_date,b.estimate,b.email,fk_client,bk_client,b.olimpo_id
--sum(nvl2(b.olimpo_id,1,0)) not_null_base, sum(nvl2(b.olimpo_id,0,1)) null_base
--,sum(nvl2(uh.olimpo_id,1,0)) not_null_uh, sum(nvl2(uh.olimpo_id,0,1)) null_uh
--,sum(nvl2(uh2.olimpo_id,1,0)) not_null_uh2, sum(nvl2(uh2.olimpo_id,0,1)) null_uh2
--,sum(nvl2(uh3.email,1,0)) not_null_uh3, sum(nvl2(uh3.email,0,1)) null_uh3
from playground.LA_hubs_kpis b
	left join playground.users_hubs uh on uh.olimpo_id = b.olimpo_id
	left join playground.users_hubs uh2 on uh2.identifier = b.email and uh.olimpo_id is null
	left join playground.dl_users_hubs_clients_missing uh3 on uh3.email = b.email and uh.olimpo_id is null and uh2.olimpo_id is null
)
, car_margin as (
	select 
	fmr.bk_sale_estimate_transaction
	,fb.bk_booking estimate
	,mvf.pnl_month 
	,mvf.pnl_date
	--Sale Price Variables
	, sum(coalesce(fmr.amt_first_publication_price_total,0)) as _first_published_price
	, sum(coalesce(fmr.amt_booking_publication_price_total,0)) as _booking_published_price
	, sum(coalesce(fmr.amt_inv_bkg_regular_publish_price, 0)) as _inv_bkg_regular_published_price
	, sum(coalesce(fmr.amt_inv_bkg_real_publish_price,0)) as _inv_bkg_real_published_price
	, sum(coalesce(fmr.amt_total_sales,0)) as _total_income
	, sum(coalesce(fmr.amt_subtotal_sales,0)) as _subtotal_income /*INCOME TOTAL*/
	, sum(coalesce(fmr.amt_tax_sales,0)) as _tax_income
	, sum(coalesce(fmr.amt_ops_autoparts_subtotal_pacman_cost,0)) as _subtotal_autoparts_pacman_cost /*AUTOPARTS COST*/
	, sum(coalesce(fmr.amt_ops_legal_subtotal_pacman_cost,0)) as _subtotal_legal_pacman_cost /*LEGAL COST*/
	, sum(coalesce(fmr.amt_subtotal_sale_cost,0)) as _subtotal_sale_cost /*PRECIO DE COMPRA DEL AUTO*/

from dwh.fact_margin_report fmr 
left join dwh.dim_fiscal_subsidiary dfs on dfs.sk_subsidiary = fmr.fk_fiscal_subsidiary 
left join dwh.dim_date dd_pnl on dd_pnl.sk_date = fmr.fk_month_for_pnl_with_deliveries  
left join dwh.dim_date dd_tran on dd_tran.sk_date = fmr.fk_transaction_date 
left join dwh.dim_car_stock dcs on dcs.sk_car_stock=fmr.fk_item_car_stock
left join dwh.dim_entity de on de.sk_entity = fmr.fk_customer_entity
left join dwh.fact_booking fb on fb.booking_tran_id=fmr.bk_sale_estimate_transaction
left join dwh.mv_finance_sales as mvf on mvf.pnl_month = CASE WHEN dfs.subsidiary_country='MX' THEN TO_CHAR(date_trunc('month',dd_pnl.date),'yyyy-mm') 
																WHEN dfs.subsidiary_country='BR' THEN TO_CHAR(date_trunc('month',dd_tran.date),'yyyy-mm')
															ELSE TO_CHAR(date_trunc('month',dd_tran.date),'yyyy-mm') END
										and mvf.bk_item = dcs.bk_item 
										and mvf.bk_entity = de.bk_entity 
								
where 1=1
		and dfs.subsidiary_country='MX' --'BR'
		and fmr.flg_invoiced=1
		and TO_CHAR(DATE_TRUNC('month', dd_pnl.date),'yyyy-mm')>='2022-01' --MX delivered Sales 2022 onwards
		--and TO_CHAR(DATE_TRUNC('month', dd_tran.date),'yyyy-mm')>='2022-01' --BR Invoiced Sales & MX Invoiced Sales before 2022
group by 1,2,3,4		
)

select distinct *,(_subtotal_income - (_subtotal_sale_cost + _subtotal_autoparts_pacman_cost + _subtotal_legal_pacman_cost)) as car_margin_numerator
from clientes c 
	left join car_margin cm on cm.estimate=c.estimate
	
