import pandas as pd
import mysql.connector
from config import db_config

# Conexión a la base de datos
mydb = mysql.connector.connect(**db_config)

# Consulta a la base de datos
mycursor = mydb.cursor()
mycursor.execute("SELECT * FROM tabla")

# Almacenamiento de los resultados en un dataframe
df = pd.DataFrame(mycursor.fetchall())
df.columns = [i[0] for i in mycursor.description]

# Impresión del dataframe
print(df.head())


--------
import pandas as pd
import mysql.connector

# Conexión a la base de datos
mydb = mysql.connector.connect(
  host="localhost",
  user="tu_usuario",
  password="tu_contraseña",
  database="tu_base_de_datos"
)

# Consulta a la base de datos
mycursor = mydb.cursor()
mycursor.execute("SELECT * FROM tabla")

# Almacenamiento de los resultados en un dataframe
df = pd.DataFrame(mycursor.fetchall())
df.columns = [i[0] for i in mycursor.description]

# Impresión del dataframe
print(df.head())


###################psycopg2

import pandas as pd
import psycopg2
from config import db_config

# Conexión a la base de datos
conn = psycopg2.connect(**db_config)

# Consulta a la base de datos
query = "SELECT * FROM tabla"
df = pd.read_sql(query, conn)

# Impresión del dataframe
print(df.head())


