from datetime import date, datetime, timedelta

import fastparquet
from sqlalchemy import create_engine, schema, Table
import pandas as pd
import numpy as np
import awswrangler as wr
import boto3
import psycopg2
import pg8000.dbapi
from config import DB_CREDS, CONN_STR, ENV
# aws_access_key_id, aws_secret_access_key, aws_session_token

boto3.setup_default_session(profile_name="biotime", region_name="us-west-2")
DEFAULT_SESSION = None
# DEFAULT_SESSION = boto3.Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, aws_session_token=aws_session_token, region_name="us-west-2")

DEFAULT_BATCH_SIZE = 50000

def _create_glue_database_if_not_exist(dbname) -> None:
    """
    Creates a database in aws glue catalog if it doesn't exist
    """
    databases = wr.catalog.databases(boto3_session=DEFAULT_SESSION)
    if dbname not in databases.values:
        wr.catalog.create_database(dbname, boto3_session=DEFAULT_SESSION)
        print(f"Database {dbname} created successfully!")
    else:
        print(f"Database {dbname} already exists")

def _fix_data_types(temp_df: "pd.DataFrame"):
    # fix mixed type, wrong datetimes and empty columns
    for col in temp_df:
        if pd.api.types.infer_dtype(temp_df[col]) in (
            "date",
            "datetime",
            "datetime64",
        ):
            mask1 = pd.to_datetime(temp_df[col], errors="coerce").isna()
            mask2 = pd.to_datetime(temp_df[col], errors="coerce").dt.date.lt(date(1900, 1, 1))

            temp_df.loc[(mask1) | (mask2), col] = pd.NaT

        if pd.api.types.infer_dtype(temp_df[col]) in (
            "decimal",
            "floating",
        ):
            temp_df[col] = temp_df[col].fillna(0).astype("float64").round(decimals=1)

        if pd.api.types.infer_dtype(temp_df[col]) in (
            "mixed",
            "empty",
            "mixed-integer",
            "mixed-integer-float",
            "string",
        ):
            temp_df[col] = temp_df[col].astype("string")

    return temp_df

def __get_dtype(column, sqltype):
    from sqlalchemy.dialects.mssql import BIT
    from sqlalchemy.types import (Integer, Float, Boolean, DateTime,
                                  Date, TIMESTAMP)

    if isinstance(sqltype, Float):
        return float
    elif isinstance(sqltype, Integer):
        # Since DataFrame cannot handle nullable int, convert nullable ints to floats
        if column.nullable:
            return float
        # TODO: Refine integer size.m
        return np.dtype('int64')
    elif isinstance(sqltype, TIMESTAMP):
        # we have a timezone capable type
        if not sqltype.timezone:
            return np.dtype('datetime64[ns]')
        return DatetimeTZDtype
    elif isinstance(sqltype, DateTime):
        # Caution: np.datetime64 is also a subclass of np.number.
        return np.dtype('datetime64[ns]')
    elif isinstance(sqltype, Date):
        return np.date
    elif isinstance(sqltype, Boolean):
        return bool
    elif isinstance(sqltype, BIT):
        # Handling database provider specific types
        return np.dtype('u1')
    # Catch all type - handle provider specific types in another elif block
    return object


def __write_parquet(batch_array, column_dict, **kwargs):
    # Create the DataFrame to hold the batch array contents
    b_df = pd.DataFrame(batch_array, columns=column_dict)
    # Cast the DataFrame columns to the sqlalchemy column analogues
    b_df = b_df.astype(dtype=column_dict)
    # Infer the equivalent Athena data types
    b_df = _fix_data_types(b_df)
    
    # Create glue database if neccesary (TODO: currently the role has not permission to do this)
    # _create_glue_database_if_not_exist(kwargs['database'])

    # Add load_date and related partition columns
    b_df['load_date'] = datetime.utcnow()
    b_df['year'] = b_df['load_date'].dt.__getattribute__('year').map(int)
    b_df['month'] = b_df['load_date'].dt.__getattribute__('month').map(int)

    # Write to the parquet file 
    print(f"Writing data to s3 location: {kwargs['path']}")
    wr.s3.to_parquet(b_df, **kwargs)
    print("Done writing table!")

def table_to_parquet(table_name: str, output_path: str, con,
                     batch_size: int = DEFAULT_BATCH_SIZE, mode: str = 'full'):

    # Set start and end dates based on exec mode
    if mode == 'full':
        start_date = datetime(1900,1,1)
        end_date = datetime.now()
    else:
        start_date =  datetime.now() - timedelta(days=2)
        end_date = datetime.now()
    
    print(f"start: {start_date}, end: {end_date}")

    # Get database schema using sqlalchemy reflection
    db_engine = create_engine(con)
    db_metadata = schema.MetaData(bind=db_engine)
    db_table = Table(table_name, db_metadata, autoload=True)

    # Get the columns for the parquet file
    column_dict = dict()
    for column in db_table.columns:
        dtype = __get_dtype(column, column.type)
        # column_dict[column.name] = dtype
        column_dict[str(column.name)] = dtype

    # Query the table
    result = db_table.select().where(db_table.c.create_time.between(start_date, end_date) | db_table.c.update_time.between(start_date, end_date)).execute()

    row_batch = result.fetchmany(size=batch_size)
    append = False
    
    while(len(row_batch) > 0):
        __write_parquet(row_batch, column_dict, 
            path=output_path, dataset=True, mode='append')
	
        # Disabled for now aws catalog tble registration
        # table='acc_transaction', database='biosecurity_postgres_global_landing', schema_evolution=True, catalog_versioning=True, partition_cols=["year", "month"],
        # projection_enabled=True, projection_types={"year": "integer", "month": "integer"}, projection_ranges={"year": "2018,2030", "month": "1,31"}, boto3_session=DEFAULT_SESSION)
        
        row_batch = result.fetchmany(size=batch_size)

    return start_date, end_date
    
if __name__ == '__main__':
    
    print(" ------ Start Main ETL Process ------ ")
    MODE='inc' #'full'
    
    print(f"Running Job in {MODE} mode")
    start_date, end_date = table_to_parquet(table_name='acc_transaction', output_path=f's3://kavak-landing-raw-{ENV}/accesslog/acc_transaction/year={datetime.now().year}/month={datetime.now().month}', con=CONN_STR, mode=MODE)
    with open(r"C:\Users\Administrator\Documents\PythonJobs\acc_transactions\execution_history.log", "w") as f:
        f.write(f"execution_time: {datetime.now()} --- date range used -> start_date: {start_date}, end_date: {end_date}")
    print(" ------ Finished Main ETL Process! ------ ")


############################################ attempt ##############################
def table_to_parquet(table_name: str, output_path: str, con,
                     batch_size: int = DEFAULT_BATCH_SIZE, mode: str = 'full'):

    # Set start and end dates based on exec mode
    if mode == 'full':
        start_date = datetime(1900,1,1)
        end_date = datetime.now()
    else:
        start_date =  datetime.now() - timedelta(days=2)
        end_date = datetime.now()
    
    print(f"start: {start_date}, end: {end_date}")

    # Create a database engine and a database connection
    db_engine = create_engine(con)
    db_conn = db_engine.connect()

    # Query the database and retrieve the data as a pandas DataFrame
    query = f"SELECT * FROM {table_name} WHERE date >= '{start_date.date()}' AND date < '{end_date.date()}'"
    df = pd.read_sql_query(query, con=db_conn)

    # Get the columns for the parquet file
    column_dict = {col: str(df[col].dtype) for col in df.columns}

    # Write the data to Parquet
    wr.s3.to_parquet(
        df,
        path=output_path,
        dataset=True,
        database=DATABASE_NAME,
        table=table_name,
        partition_cols=['date'],
        mode='overwrite',
        boto3_session=DEFAULT_SESSION,
    )

    print("Done writing table!")
