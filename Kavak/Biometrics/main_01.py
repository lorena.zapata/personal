import pandas as pd
import psycopg2
from config import DB_CREDS
from datetime import date, datetime, timedelta

query= """
with base as
	(select 
	--row_number() over (partition by at2.pers_person_pin||at2.att_date order by 1 desc) as rn,
	at2.auth_area_name,
	at2.auth_area_no,
	at2.pers_person_pin, 
	at2.pers_person_name,
	at2.pers_person_last_name,
	at2.auth_dept_code,
	at2.auth_dept_name,
	at2.att_date,
	at2.att_datetime,
	at2.att_state,
	--att_time,
	--att_verify,
	at2.device_id,
	at2.device_name,
	at2.device_sn,
	per.wo_id
	from att_transaction at2 
	left join (select
	           pc.person_id,
	           pc.person_pin,
	           pae.attr_value6 as wo_id
	           from pers_card pc
	           left join pers_attribute_ext pae on pae.person_id = pc.person_id) per on  case when char_length(per.person_pin)>6 then per.person_pin else lpad(per.person_pin,6,'0') end = case when char_length(at2.pers_person_pin)>6 then at2.pers_person_pin else lpad(at2.pers_person_pin,6,'0') end   
	--where at2.auth_area_name = 'MEXICO'
	--and at2.pers_person_pin in ('4581') 
	order by 11 desc)
select 
    b.auth_area_name,
	b.auth_area_no,
	b.pers_person_pin, 
	b.pers_person_name,
	b.pers_person_last_name,
	b.auth_dept_code,
	b.auth_dept_name,
	to_date(b.att_date ,'yyyy-mm-dd'),
	min(b.att_datetime) as first_in,
	max(b.att_datetime) as last_in,
	max(b.att_datetime) - min(b.att_datetime) as work_time,
	--b.att_state,
	det.card_count,
	det.att_times,
	--att_time,
	--att_verify,
	b.device_id,
	b.device_name,
	null,
	b.device_sn,
	b.wo_id
from base b
left join att_day_card_detail det on case when char_length(det.pers_person_pin)>6 then det.pers_person_pin else lpad(det.pers_person_pin,6,'0') end||det.att_date = case when char_length(b.pers_person_pin)>6 then b.pers_person_pin else lpad(b.pers_person_pin,6,'0') end||b.att_date
where to_date(b.att_date, 'YYYY-MM-DD') >= current_date-15
group by 1,2,3,4,5,6,7,8,12,13,14,15,16,17,18
union
select 
null,
null,
pin,
"name",
last_name,
dept_code,
dept_name,
date(first_in_time) as grabar_fecha, 
first_in_time as first_in_time,
last_out_time as last_out_time,
last_out_time - first_in_time as work_time,
null,
null,
null,
reader_name_in, 
reader_name_out,
null,
per.wo_id
from acc_firstin_lastout afl 
left join (select
           pc.person_id,
           pc.person_pin,
           pae.attr_value6 as wo_id
           from pers_card pc
           left join pers_attribute_ext pae on case when char_length(pae.person_id)>6 then pae.person_id else lpad(pae.person_id,6,'0') end = case when char_length(pc.person_pin)>6 then pc.person_pin else lpad(pc.person_pin,6,'0') end) per on case when char_length(per.person_pin)>6 then per.person_pin else lpad(per.person_pin,6,'0') end = case when char_length(afl.pin)>6 then afl.pin else lpad(afl.pin,6,'0') end 
"""



# Conexión a la base de datos
conn = psycopg2.connect(**DB_CREDS)
query_test = """select date(first_in_time) from acc_firstin_lastout"""

def get_result(query,limit: str = '', mode: str = 'full'):

 # Set start and end dates based on exec mode
    if mode == 'full':
        start_date = datetime(2022,1,1)
        end_date = datetime.now()
    else:
        start_date =  datetime.now() - timedelta(days=2)
        end_date = datetime.now()
    
    print(f"start: {start_date}, end: {end_date}")

    query= query+" where (first_in_time) between '"+ str(start_date) +"' and '" + str(end_date)+"' "+(limit)

    temp_df = pd.read_sql(query, conn)

    pd.DataFrame(temp_df).fillna('').astype(str)
    temp_df.replace("NaT","", inplace=True)
    return temp_df

def _fix_data_types(temp_df: "pd.DataFrame"):
    # fix mixed type, wrong datetimes and empty columns
    for col in temp_df:
        if pd.api.types.infer_dtype(temp_df[col]) in (
            "date",
            "datetime",
            "datetime64",
        ):
            mask1 = pd.to_datetime(temp_df[col], errors="coerce").isna()
            mask2 = pd.to_datetime(temp_df[col], errors="coerce").dt.date.lt(date(1900, 1, 1))

            temp_df.loc[(mask1) | (mask2), col] = pd.NaT

        if pd.api.types.infer_dtype(temp_df[col]) in (
            "decimal",
            "floating",
        ):
            temp_df[col] = temp_df[col].fillna(0).astype("float64").round(decimals=1)

        if pd.api.types.infer_dtype(temp_df[col]) in (
            "mixed",
            "empty",
            "mixed-integer",
            "mixed-integer-float",
            "string",
        ):
            temp_df[col] = temp_df[col].astype("string")

    return temp_df


df= get_result(query=query, mode= '', limit= 'limit 10')
df=_fix_data_types(df)
