
--test
/*
SELECT max(update_date),max(fk_application_date) FROM dwh.fact_loan_request
SELECT * FROM dwh.fact_loan_request where flag_deleted_record is false -- booking_id=16998082
SELECT fk_handoff_date,fk_provider_approval_date,flag_main_loan_request,* FROM dwh.fact_loan_request WHERE booking_id=480673
SELECT * FROM dwh.fact_global_kc limit 1
*/
---------------------------------------------------------Total Query---------------------------------------------------------

with handoffs as (
----------------------HANDOFF----------------------

SELECT 	cast(left(j.fk_date,6)||'01'as int) AS fk_date,
	    --j.booking_id,
		--fk_client_geography AS fk_geo,
		country_iso,
	    count(DISTINCT(j.booking_id)) AS unique_handoff
		
	FROM (
		select booking_id,fk_date from (
					    SELECT booking_id, fk_handoff_date as fk_date,handoff_time
					    ,row_number() over(partition by booking_id order by handoff_time asc) as rn FROM dwh.fact_loan_request fl
						WHERE flag_deleted_record is false and fl.flag_main_loan_request and fk_payment_type=23 and flag_handoff is true-- and booking_id=18017557
										)j_0
		where rn=1
		) j
	    
	LEFT JOIN dwh.fact_loan_request lr ON j.booking_id=lr.booking_id and flag_handoff is true and flag_deleted_record is false
	INNER JOIN dwh.fact_booking  fb ON fb.bk_booking = lr.booking_id
	LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=fk_client_geography 
		
	GROUP BY 1,2--,3
	
)
, t_handoffs as (

	SELECT cast(left(fk_handoff_date,6)||'01'as int) AS fk_date,country_iso
	--,fk_handoff_date,handoff_time,booking_id,loan_request_id,flag_deleted_record,flag_main_loan_request,fk_payment_type,flag_handoff
	,count(*) as total_handoff
	FROM dwh.fact_loan_request fl
		LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=fk_client_geography 
	WHERE 1=1
	and flag_deleted_record is false
	and fk_payment_type=23 
	and fk_handoff_date is not null
	group by 1,2
)
, bank_approvals as(
----------------------BANK APPROVAL----------------------
	with totals as(
	SELECT 
			country_iso,booking_id,fk_first_application_date, fk_provider_approval_date as fk_date,provider_approval_time,fk_credit_conditions
			,row_number() over(partition by booking_id order by provider_approval_time desc) as rn
		FROM dwh.fact_loan_request lr
		    	LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=lr.fk_client_geography
		WHERE flag_deleted_record is false and flag_deleted_record is false and flag_provider_approval IS TRUE 
		AND flag_main_loan_request is true and lr.fk_payment_type=23 --and booking_id=2937603
		--and fk_provider_approval_date > 20220301
	)	
	, totals_duplicated as (
	
		select country_iso,cast(left(fk_date,6) as int) fk_date,count(booking_id) bank_approval from totals group by 1,2
	)
	, totals_unique as (
	
		select country_iso,booking_id,fk_date,fk_credit_conditions from totals where rn=1 
	)
	
	--select booking_id,count(*) from totals group by booking_id having count(*)>2 order by 2 desc
	--select * from totals
	
	SELECT
	
			tu.country_iso,cast(left(tu.fk_date,6)||'01' as int) fk_date
			,td.bank_approval
			,SUM(case when sc.bk_credit_condition is not null then 1 else 0 end) as same_conditions
			,SUM(case when nc.bk_credit_condition is not null then 1 else 0 end) as new_conditions
			,SUM(case when un.bk_credit_condition is not null then 1 else 0 end) as unk_conditions
			,same_conditions+new_conditions+unk_conditions unique_bank_approval
		FROM totals_unique tu
			LEFT JOIN totals_duplicated td on td.fk_date=cast(left(tu.fk_date,6) as int) and td.country_iso=tu.country_iso
		    LEFT JOIN dwh.dim_credit_condition sc on tu.fk_credit_conditions = sc.sk_credit_condition and sc.bk_credit_condition  IN (1,3)
		    LEFT JOIN dwh.dim_credit_condition nc on tu.fk_credit_conditions = nc.sk_credit_condition and nc.bk_credit_condition  IN (2,4,5,6,7)
		    LEFT JOIN dwh.dim_credit_condition un on tu.fk_credit_conditions = un.sk_credit_condition and un.bk_credit_condition  NOT IN (1,2,3,4,5,6,7)
		    
	
		GROUP BY 1,2,3
)
, CCR_bk_hnd as (
---------------------- CCR unique booking to unique handoff - to_date & lags ----------------------
	
	with unique_handoff as (
					select booking_id,h_date from  (
						select booking_id, fk_handoff_date as h_date,handoff_time
						,row_number() over(partition by booking_id order by handoff_time asc) as rn FROM dwh.fact_loan_request fl
						WHERE flag_deleted_record is false and fl.flag_main_loan_request and fk_payment_type=23 and flag_handoff is true--and booking_id=18017557
						--and fk_booking_creation_date > 20220500
						) h
						where rn=1
	)
	
	,CCR_S01 as(
	select 
	country_iso,bk_booking,uh.booking_id
	,fb.fk_first_booking_creation_date as b_date, uh.h_date
	,DATE(h_date)-DATE(b_date) diff_h_b
	,case when diff_h_b <=3 then true else false end as flag_3d
	,case when diff_h_b <=7 then true else false end as flag_7d
	,case when diff_h_b <=15 then true else false end as flag_15d
	,case when diff_h_b >15 then true else false end as flag_to_date
	
	from dwh.fact_booking fb
			
		left join unique_handoff uh on uh.booking_id=fb.bk_booking
		left join dwh.dim_geography dm ON dm.sk_geography=fb.fk_client_geo
		
		where fk_payment_type=23 and fb.flag_main_booking=1 
		-- and fb.fk_first_booking_creation_date >= 20220100 and fb.fk_first_booking_creation_date < 20220700
	
	)
	
	select 
	country_iso--,b_date as fk_date
	,cast(left(b_date,6)||'01' as int) as fk_date
	--,cast(left(b_date,6) as int)
	--,bk_booking
	,count(distinct bk_booking) denominator_bk
	,count(distinct booking_id) denominator_bk_h
	,sum(case when h_date is not null and flag_3d then 1 else 0 end) numerator_3
	,sum(case when h_date is not null and flag_7d then 1 else 0 end) numerator_7
	,sum(case when h_date is not null and flag_15d then 1 else 0 end) numerator_15
	,sum(case when h_date is not null and flag_to_date then 1 else 0 end) numerator_30
	,(cast(denominator_bk_h as float) / nullif (cast(denominator_bk as float),0) ) as CCR_to_date_handoff_total_bookings
	,(cast(numerator_7 as float) / nullif (cast(denominator_bk as float),0) ) as CCR_7_handoff_total_bookings
	,(cast(numerator_15 as float) / nullif (cast(denominator_bk as float),0) ) as CCR_15_handoff_total_bookings
	,(cast(numerator_30 as float) / nullif (cast(denominator_bk as float),0) ) as CCR_30_handoff_total_bookings
	-- booking con handoff
	,(cast(numerator_3 as float) / nullif (cast(denominator_bk_h as float),0) ) as CCR_3_handoff_bookings
	,(cast(numerator_7 as float) / nullif (cast(denominator_bk_h as float),0) ) as CCR_7_handoff_bookings
	,(cast(numerator_15 as float) / nullif (cast(denominator_bk_h as float),0) ) as CCR_15_handoff_bookings
	,(cast(numerator_30 as float) / nullif (cast(denominator_bk_h as float),0) ) as CCR_to_date_handoff_bookings
	
	
	from ccr_s01
	where 1=1
	--and fk_date = 20220501
	and country_iso = 'MX'
	group by 1,2
)

, CCR_hnd_ba as(	
---------------------- CCR unique handoff to unique bank approval - to_date & lags ----------------------
	with unique_handoff as (
					select country_iso,booking_id,fk_date from  (
						select country_iso,booking_id, fk_handoff_date as fk_date,handoff_time
						,row_number() over(partition by booking_id order by handoff_time asc) as rn FROM dwh.fact_loan_request fl
						LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=fl.fk_client_geography
						WHERE flag_deleted_record is false and fl.flag_main_loan_request and fk_payment_type=23 and flag_handoff is true--and booking_id=18017557
						--and fk_booking_creation_date > 20220500
						) h
						where rn=1
	)
	,unique_bank_approval as (
	--bank approval
	select * from (
		SELECT 
			country_iso,booking_id,fk_first_application_date, fk_provider_approval_date as fk_date,provider_approval_time,fk_credit_conditions
			,row_number() over(partition by booking_id order by provider_approval_time desc) as rn
		FROM dwh.fact_loan_request lr
		    	LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=lr.fk_client_geography
		WHERE flag_deleted_record is false and flag_deleted_record is false and flag_provider_approval IS TRUE 
		AND flag_main_loan_request is true and lr.fk_payment_type=23 --and booking_id=2937603
		--and fk_provider_approval_date > 20220301
		) ub where rn=1
		
		)
	
	,CCR_S01 as(
	
	select 
	uf.country_iso
	,uf.booking_id as booking_id_h,ba.booking_id as booking_id_ba
	,uf.fk_date as h_date, ba.fk_date as ba_date
	,DATE(ba_date)-DATE(h_date) diff_ba_h
	,case when diff_ba_h <=7 then true else false end as flag_7d
	,case when diff_ba_h <=15 then true else false end as flag_15d
	,case when diff_ba_h <=30 then true else false end as flag_30d
	
	from unique_handoff uf
		left join unique_bank_approval ba on uf.booking_id=ba.booking_id
		-- and fb.fk_first_booking_creation_date >= 20220100 and fb.fk_first_booking_creation_date < 20220700
	
	)
	
	select 
	country_iso--,b_date as fk_date
	,cast(left(h_date,6)||'01' as int) as fk_date
	--,cast(left(b_date,6) as int)
	--,bk_booking
	,count(distinct booking_id_h) denominator_h
	,count(distinct booking_id_ba) denominator_ba
	,sum(case when h_date is not null and flag_7d then 1 else 0 end) numerator_7
	,sum(case when h_date is not null and flag_15d then 1 else 0 end) numerator_15
	,sum(case when h_date is not null and flag_30d then 1 else 0 end) numerator_30
	,(cast(denominator_ba as float) / nullif (cast(denominator_h as float),0) ) as CCR_to_date_handoff_total_ba
	,(cast(numerator_7 as float) / nullif (cast(denominator_h as float),0) ) as CCR_7_handoff_total_ba
	,(cast(numerator_15 as float) / nullif (cast(denominator_h as float),0) ) as CCR_15_handoff_total_ba
	,(cast(numerator_30 as float) / nullif (cast(denominator_h as float),0) ) as CCR_30_handoff_total_ba
	-- handoff con bank_approval
	,(cast(numerator_7 as float) / nullif (cast(denominator_ba as float),0) ) as CCR_7_handoff_ba
	,(cast(numerator_15 as float) / nullif (cast(denominator_ba as float),0) ) as CCR_15_handoff_ba
	,(cast(numerator_30 as float) / nullif (cast(denominator_ba as float),0) ) as CCR_30_handoff_ba
	
	
	from ccr_s01
	where 1=1
	--and fk_date = 20220501
	--and country_iso = 'MX'
	group by 1,2
	)
		-----------------------------CLIENT APPROVAL----------------------
,client_approvals as (

			 select 
			 
			 cast(fk_date||'01' as int) fk_date,country_iso,count(distinct booking_id) client_approval 
			 --booking_id,fk_date,fk_client_approval_date,fk_booking_declared_sale_date
			 from (
			    SELECT 
			   booking_id
			   ,country_iso
			    ,fk_client_approval_date,fk_booking_declared_sale_date
				,left(case 	
			    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is not null and 
			    	fk_client_approval_date <= fk_booking_declared_sale_date then fk_client_approval_date
			    	
			    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is not null and 
			    	fk_client_approval_date > fk_booking_declared_sale_date then fk_booking_declared_sale_date
			    	
			    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is null then fk_client_approval_date
			    	when fk_client_approval_date is null and fk_booking_declared_sale_date is not null then fk_booking_declared_sale_date
			    	
			    	else -1
			    end,6) fk_date
			
			    ,row_number() over(partition by booking_id order by fk_client_approval_date,fk_booking_declared_sale_date asc) as rn 
			
			    FROM dwh.fact_loan_request fl
			     LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=fk_client_geography 
									
			    WHERE 1=1
				and flag_deleted_record is false and fl.flag_main_loan_request and fk_payment_type=23 
				and (fk_client_approval_date IS NOT null or fk_booking_declared_sale_date is not null)
				and country_iso='MX'
				--and fk_date>=202201
			  	--and booking_id=14394415--13403425--1682016
					
			) as ca 
		where rn=1 
		group by 1,2

)
-----------------------------CLIENT APPROVAL BANK APPROVAL COHORTED----------------------
,client_approval_bac as(
	
	with unique_bank_approval as (
	--bank approval
	select * from (
		SELECT 
			country_iso,booking_id,fk_first_application_date, fk_provider_approval_date as fk_date,provider_approval_time,fk_credit_conditions
			,row_number() over(partition by booking_id order by provider_approval_time desc) as rn
		FROM dwh.fact_loan_request lr
		    	LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=lr.fk_client_geography
		WHERE flag_deleted_record is false and flag_deleted_record is false and flag_provider_approval IS TRUE 
		AND flag_main_loan_request is true and lr.fk_payment_type=23 --and booking_id=2937603
		--and fk_provider_approval_date > 20220301
		) ub where rn=1
		
		)
	,client_approvals as (

			    SELECT 
			   booking_id
			   ,country_iso
			    ,fk_client_approval_date,fk_booking_declared_sale_date
				,case 	
			    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is not null and 
			    	fk_client_approval_date <= fk_booking_declared_sale_date then fk_client_approval_date
			    	
			    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is not null and 
			    	fk_client_approval_date > fk_booking_declared_sale_date then fk_booking_declared_sale_date
			    	
			    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is null then fk_client_approval_date
			    	when fk_client_approval_date is null and fk_booking_declared_sale_date is not null then fk_booking_declared_sale_date
			    	
			    	else -1
			    end fk_date
			
			    ,row_number() over(partition by booking_id order by fk_client_approval_date,fk_booking_declared_sale_date asc) as rn 
			
			    FROM dwh.fact_loan_request fl
			     LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=fk_client_geography 
									
			    WHERE 1=1
				and flag_deleted_record is false and fl.flag_main_loan_request and fk_payment_type=23 
				and (fk_client_approval_date IS NOT null or fk_booking_declared_sale_date is not null)
				and country_iso='MX'
				--and fk_date>=202201
			  	--and booking_id=14394415--13403425--1682016

)
	,CCR_S01 as(
	
	select 
	ba.country_iso
	,ba.booking_id as booking_id_ba,ca.booking_id as booking_id_ca
	,ba.fk_date as ba_date, ca.fk_date as ca_date
	,DATE(ca_date)-DATE(ba_date) diff_ba_ca
	,case when diff_ba_ca <=7 then true else false end as flag_7d
	,case when diff_ba_ca <=15 then true else false end as flag_15d
	,case when diff_ba_ca <=30 then true else false end as flag_30d
	
	from unique_bank_approval ba
		left join client_approvals ca on ba.booking_id=ca.booking_id and ca.rn=1
		-- and fb.fk_first_booking_creation_date >= 20220100 and fb.fk_first_booking_creation_date < 20220700
	
	)
	
	select 
	country_iso--,ba_date as fk_date
	,cast(left(ba_date,6)||'01' as int) as fk_date
	--,ca_date
	--,booking_id_ba,booking_id_ca
	,count(distinct booking_id_ba) denominator_ba
	,count(distinct booking_id_ca) denominator_ca
	,sum(case when ca_date is not null and flag_7d then 1 else 0 end) numerator_7
	,sum(case when ca_date is not null and flag_15d then 1 else 0 end) numerator_15
	,sum(case when ca_date is not null and flag_30d then 1 else 0 end) numerator_30
	,(cast(denominator_ca as float) / nullif (cast(denominator_ba as float),0) ) as CCR_to_date_ca_total_ba
	,(cast(numerator_7 as float) / nullif (cast(denominator_ba as float),0) ) as CCR_7_ca_total_ba
	,(cast(numerator_15 as float) / nullif (cast(denominator_ba as float),0) ) as CCR_15_ca_total_ba
	,(cast(numerator_30 as float) / nullif (cast(denominator_ba as float),0) ) as CCR_30_ca_total_ba
	-- bank_approval con client_approval
	,(cast(numerator_7 as float) / nullif (cast(denominator_ca as float),0) ) as CCR_7_ca_ba
	,(cast(numerator_15 as float) / nullif (cast(denominator_ca as float),0) ) as CCR_15_ca_ba
	,(cast(numerator_30 as float) / nullif (cast(denominator_ca as float),0) ) as CCR_30_ca_ba
	
	
	from ccr_s01
	where 1=1
	--and fk_date = 20220101
	--and country_iso = 'MX'
	group by 1,2--,3,4,5
	)
-----------------------------AVG LEAD TIMES----------------------	
,ALT as (
					with 
						client_approvals as (
						select * from (
									    SELECT 
									   booking_id
									   ,country_iso
									    ,fk_client_approval_date,fk_booking_declared_sale_date
										,case 	
									    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is not null and 
									    	fk_client_approval_date <= fk_booking_declared_sale_date then fk_client_approval_date
									    	
									    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is not null and 
									    	fk_client_approval_date > fk_booking_declared_sale_date then fk_booking_declared_sale_date
									    	
									    	when fk_client_approval_date is not null and fk_booking_declared_sale_date is null then fk_client_approval_date
									    	when fk_client_approval_date is null and fk_booking_declared_sale_date is not null then fk_booking_declared_sale_date
									    	
									    	else -1
									    end ca_date
									
									    ,row_number() over(partition by booking_id order by fk_client_approval_date,fk_booking_declared_sale_date asc) as rn 
									
									    FROM dwh.fact_loan_request fl
									     LEFT JOIN dwh.dim_geography dm ON dm.sk_geography=fk_client_geography 
															
									    WHERE 1=1
										and flag_deleted_record is false and fl.flag_main_loan_request and fk_payment_type=23 
										and (fk_client_approval_date IS NOT null or fk_booking_declared_sale_date is not null)
										--and country_iso='MX'
										--and fk_date>=202201
									  	--and booking_id=14394415--13403425--1682016
										)
										 a where rn=1
										 )
						,min_dates as (
											select dm.country_iso
											,fl.booking_id
											--,cast(left(ba_date,6) as int) as fk_date
											--,h_date,b_date
											,min(DATE(fb.fk_first_booking_creation_date)) min_b_date
											,min(DATE(fl.fk_booking_creation_date)) min_last_b_date
											,min(DATE(fk_handoff_date)) min_h_date
											,min(handoff_time) min_h_date_time
											,min(provider_approval_time) min_ba_date_time
											,min(DATE(ca_date)) min_ca_date
											/*from  (
												select fl.booking_id, fk_handoff_date as h_date,handoff_time,fk_booking_creation_date b_date,country_iso
												,fk_provider_approval_date as ba_date ,provider_approval_time,ca_date
												--,row_number() over(partition by booking_id order by provider_approval_time asc) as rn 
												*/
											FROM dwh.fact_loan_request fl
												left join dwh.fact_booking as fb on fb.bk_booking = fl.booking_id and fb.flag_main_booking 
												left join client_approvals ca on ca.booking_id=fl.booking_id 
												left join dwh.dim_geography dm ON dm.sk_geography=fl.fk_client_geography 
												WHERE fl.flag_deleted_record is false and fl.flag_main_loan_request and fl.fk_payment_type=23 
												
												-- and booking_id=15637023
												--and fk_booking_creation_date > 20220500
												
												--where rn=1 --and fk_date in (20210101)
												group by 1,2--,3,4,5
							)
						, ALT as (
							select 
							country_iso,booking_id,min_b_date,min_h_date_time,min_ba_date_time,min_ca_date
							,to_char(min_b_date,'YYYYMM') fk_b_date
							,to_char(min_last_b_date,'YYYYMM') fk_last_b_date
							,to_char(min_h_date_time,'YYYYMM') fk_h_date
							,to_char(min_ba_date_time,'YYYYMM') fk_ba_date
							,to_char(min_ca_date,'YYYYMM') fk_ca_date
							,datediff(hour,min_b_date,min_h_date) b_h
							,datediff(hour,min_last_b_date,min_h_date) last_b_h
							,datediff(hour,min_h_date_time,min_ba_date_time) h_ba
							,datediff(hour,min_ba_date_time,min_ca_date) ba_ca
							from min_dates
							where 1=1
							--and booking_id in (15669489)
							--and fk_b_date >=202201
							)
						,ALT_b_h as(
							select country_iso,fk_h_date as fk_date ,avg(b_h)::float/24 avg_b_h from ALT where b_h>=0 group by 1,2
						)
						,ALT_last_b_h as(
							select country_iso,fk_h_date as fk_date ,avg(last_b_h)::float/24 as avg_last_b_h from ALT where b_h>=0 group by 1,2
						)
						,ALT_h_ba as(
							select country_iso,fk_ba_date as fk_date ,avg(h_ba)::float/24 avg_h_ba from ALT where h_ba>=0 group by 1,2
						)
						,ALT_ba_ca as(
							select country_iso,fk_ca_date as fk_date ,avg(ba_ca)::float/24 avg_ba_ca from ALT where ba_ca>=0 group by 1,2
						)
						--select * from ALT where fk_ca_date=202206
						select 
						a.country_iso,(a.fk_date||'01')::int as fk_date
						,trunc(avg_b_h,2) avg_b_h
						,trunc(avg_last_b_h,2) avg_last_b_h
						,trunc(avg_h_ba,2) avg_h_ba
						,trunc(avg_ba_ca,2) avg_ba_ca
						from ALT_b_h a
							left join ALT_h_ba b on b.country_iso=a.country_iso and b.fk_date=a.fk_date
							left join ALT_ba_ca c on c.country_iso=a.country_iso and c.fk_date=a.fk_date
							left join ALT_last_b_h d on d.country_iso=a.country_iso and d.fk_date=a.fk_date
						where a.country_iso is not null and a.fk_date is not null
	)
-----------------------------FINANCING SALES----------------------
, f_sales as (

select 
--booking_id,
country_iso,
left(fb.fk_delivery_date,6)::int as fk_date --,row_number() over(partition by booking_id order by handoff_time asc) as rn 
,SUM(case when UPPER(fp."name") !='KAVAK FINANCIAMIENTO' then 1 else null end) as f_sales_non_kc
,SUM(case when UPPER(fp."name") ='KAVAK FINANCIAMIENTO' then 1 else null end) as f_sales_kc_old
,f_sales_non_kc + f_sales_kc_old as total_f_sales
,f_sales_kc_old::float/total_f_sales::float kc_sales_f_sales_old

from dwh.fact_loan_request fl
inner join dwh.fact_booking fb ON fb.bk_booking = fl.booking_id and fb.fk_delivery_date is not null
left join dwh.dim_geography dm ON dm.sk_geography=fk_client_geography 
left join dwh.dim_financing_provider fp on fp.sk_financing_provider=fl.fk_financing_provider				    

where 1=1 
and fl.flag_deleted_record is false 
and fl.flag_main_loan_request 
and fl.fk_payment_type=23 
and fl.flag_selected_provider = 'true'
and dm.country_iso = 'MX'
-- and booking_id=18017557
group by 1,2

)
, f_sales_new as (
SELECT
	dg.country_iso
	,dd.year_month as fk_date
	--,dpt."type"
	--,fb.bk_booking
	--,UPPER(fp."name")
	,COUNT(DISTINCT case WHEN fmr.qty_units_by_item_customer >= 1 THEN (fmr.fk_item_car_stock || '-' || fmr.fk_customer_entity) END) AS total_sales
	,COUNT(DISTINCT case WHEN fmr.qty_units_by_item_customer >= 1 and dpt."type"!='Financing'  THEN (fmr.fk_item_car_stock || '-' || fmr.fk_customer_entity) END) AS cash_sales
	,COUNT(DISTINCT case WHEN fmr.qty_units_by_item_customer >= 1 and dpt."type"='Financing'  THEN (fmr.fk_item_car_stock || '-' || fmr.fk_customer_entity) END) AS f_sales
	,COUNT(DISTINCT case WHEN fmr.qty_units_by_item_customer >= 1 and UPPER(fp."name") !='KAVAK FINANCIAMIENTO' then (fmr.fk_item_car_stock || '-' || fmr.fk_customer_entity) else null end) as f_sales_non_kc
	,COUNT(DISTINCT case WHEN fmr.qty_units_by_item_customer >= 1 and UPPER(fp."name") ='KAVAK FINANCIAMIENTO' then (fmr.fk_item_car_stock || '-' || fmr.fk_customer_entity) else null end) as f_sales_kc
	,f_sales::float/total_sales::float f_sales_totals
	,f_sales_kc::float/total_sales::float kc_sales_totals
	,f_sales_kc::float/f_sales::float kc_sales_f_sales

FROM
	dwh.fact_booking fb
		INNER join dwh.dim_payment_type dpt on dpt.sk_payment_type =fb.fk_payment_type 	
    	inner join dwh.fact_margin_report fmr ON fb.booking_tran_id = fmr.bk_sale_estimate_transaction
		INNER join dwh.dim_entity AS e ON fmr.fk_customer_entity = e.sk_entity
		INNER join dwh.dim_geography dg ON fb.fk_client_geo = dg.sk_geography
		INNER join dwh.dim_date dd ON fb.fk_delivery_date = dd.sk_date
		left join dwh.fact_loan_request fl on fl.booking_id = fb.bk_booking and fl.flag_selected_provider = 'true' and fl.flag_deleted_record is false and fl.fk_payment_type=23
		left join dwh.dim_financing_provider fp on fp.sk_financing_provider=fl.fk_financing_provider

WHERE
  dg.country_iso = 'MX'
  AND fb.flag_delivered IS TRUE
  AND e.flag_test_gsheet IS false
  --and year_month=202207
  --and fb.bk_booking in (18016632)
  --and dpt."type"='Financing'-- and UPPER(fp."name") is not null

GROUP BY 1,2


)
-----------------------------LOAN TERM, INTEREST RATE, LOAN TO VALUE----------------------
, loan as (

	with 
	details as (
	select 
	    flr.booking_id 
	  , dm.country_iso
	  , flr.loan_request_id  
	  , fb.amt_sales_invoice  - fb.amt_sales_tax as booking_precio_venta 
	  , dt.purchase_order_total as bo_precio_compra
	  , coalesce(kflr.financing_amount, flr.amt_loan_total, kflr.total_financing_amount) as amt_loan
	  ,case when UPPER(fp."name") like '%KAVAK FINANCIAMIENTO%' then amt_loan else null end amt_loan_kc 
	  ,case when UPPER(fp."name") like '%KAVAK FINANCIAMIENTO%' then booking_precio_venta else null end booking_precio_venta_kc
	  ,case when UPPER(fp."name") like '%KAVAK FINANCIAMIENTO%' then bo_precio_compra else null end bo_precio_compra_kc
	  --, left(fb.fk_delivery_date, 6)::int as fk_date
	  , fb.fk_delivery_date as fk_date
	  --, coalesce(pfvl.list_item_name, REGEXP_SUBSTR(kflr.financing_term_ho, '[0-9]+[0-9]')) as plazo
	  , NULLIF (REGEXP_SUBSTR(kflr.financing_term_ho, '[0-9]+[0-9]'),'')::int as plazo
	  ,case when UPPER(fp."name") like '%KAVAK FINANCIAMIENTO%' then NULLIF (REGEXP_SUBSTR(kflr.financing_term_ho, '[0-9]+[0-9]'),'')::int else null end plazo_kc
	  , bso.inventory_item_id
	  , amt_loan / booking_precio_venta as amt_loan_sold
	  , amt_loan / bo_precio_compra as amt_loan_purchased
	  --, kflr.financing_term_v2_id
	  --, kflr.formalization_deadline_id
	  --, kflr.financing_term_ho
	  				--,nullif (kflr.formalization_fee::float,'') form_fee
	  ,kflr.formalization_fee
	  ,case when kflr.formalization_fee::float/100 >1 then null else kflr.formalization_fee::float/100 end form_fee
	  ,case when UPPER(fp."name") like '%KAVAK FINANCIAMIENTO%' then form_fee else null end form_fee_kc
	  ,flr.amt_loan_interest_rate
	  ,case when flr.amt_loan_interest_rate::float/100 >1 then null else flr.amt_loan_interest_rate::float/100 end loan_fee
	  ,case when UPPER(fp."name") like '%KAVAK FINANCIAMIENTO%' then loan_fee else null end loan_fee_kc
	  ,kflr.rate_ho
	  ,nullif(REGEXP_SUBSTR(replace(kflr.rate_ho,',','.'),'[+-]?([0-9]*[.])?[0-9]+'),'')::float ho_fee_raw
	  ,case when ho_fee_raw/100 >1 then null else ho_fee_raw/100 end ho_fee
	  ,case when UPPER(fp."name") like '%KAVAK FINANCIAMIENTO%' then ho_fee else null end ho_fee_kc
	 ,coalesce(form_fee,loan_fee,ho_fee) rate_fee
	 ,case when UPPER(fp."name") like '%KAVAK FINANCIAMIENTO%' then coalesce(form_fee,loan_fee,ho_fee) else null end rate_fee_kc
	from dwh.fact_loan_request flr
	left join dwh.fact_booking fb on flr.booking_id  = fb.bk_booking 
	left join dwh.dim_geography dm on dm.sk_geography=fk_client_geography
	left join stg.kc_loan_request_prep kflr on flr.loan_request_id  = kflr.loan_request_id 
	--left join spectrum_netsuite_csd.plazo_financiamiento_v2_list pfvl on coalesce(kflr.financing_term_v2_id, kflr.formalization_deadline_id) = pfvl.list_id
	left join stg.booking_prep bp on fb.bk_booking = bp.bk_booking 
	left join stg.booking_sales_order bso  on bp.sale_order_id  = bso.transaction_id 
	left join stg.data_tape dt on bso.inventory_item_id = dt.purchase_order_item_id
	left join dwh.dim_financing_provider fp on fp.sk_financing_provider=flr.fk_financing_provider
	--left join stg.kc_loan_request_prep klrp on klrp.loan_request_id = flr.loan_request_id 
	where fb.fk_delivery_date is not null
	  and flr.flag_selected_provider = 'true'
	  and flr.flag_deleted_record is false 
	  and flr.flag_main_loan_request and flr.fk_payment_type=23
	  and dm.country_iso = 'MX'
	 -- and flr.booking_id  IN (18069349,18003552) --(16027969,16048954,14992240,15063763,15580363,16011619,15891537,15895870,16450600,16452233)
	  --and flr.booking_id in (386249,530670)
	  -- and kflr.rate_ho like ('%"%"%') 
	  --limit 10
	  --and fk_date >= 20220501
	  --and fk_date <  20220601
	  --and amt_loan is null
	)  
	,totals as (
			select 
				 country_iso
			     ,left(fk_date, 6)::int as fk_date
			     ,sum(booking_precio_venta) as total_sold_value 
			     ,sum(booking_precio_venta_kc) as total_sold_value_kc
			     ,sum(bo_precio_compra) as total_purchase_value 
			     ,sum(bo_precio_compra_kc) as total_purchase_value_kc
	     		 ,sum(amt_loan) as amt_loan_total
	     		 ,sum(amt_loan_kc) as amt_loan_total_kc
	     		 ,avg(plazo) as avg_plazo_raw
	     		 ,avg(case when plazo<=120 and plazo is not null then plazo else null end) as avg_loan_term
	     		 ,avg(case when plazo_kc<=120 and plazo_kc is not null then plazo_kc else null end) as avg_loan_term_kc
	     		 ,avg(form_fee) form_fee
	     		 ,avg(form_fee_kc) form_fee_kc
	     		 ,avg(loan_fee) loan_fee
	     		 ,avg(loan_fee_kc) loan_fee_kc
	     		 ,avg(ho_fee) ho_fee
	     		 ,avg(ho_fee_kc) ho_fee_kc
	     		 ,avg(rate_fee) rate_fee
	     		 ,avg(rate_fee_kc) rate_fee_kc
	     		 ,sum(case when rate_fee is null then 1 else 0 end) interest_rate_null
	     		 ,sum(case when rate_fee is not null then 1 else 0 end) interest_rate_not_null
			     ,SUM(CASE WHEN amt_loan IS null then 1 ELSE 0 END)  as null_amt_loan
			     ,SUM(case when amt_loan_sold>1 then 1 else 0 end) "sold_>1"
				 ,SUM(case when amt_loan_sold<=0 then 1 else 0 end) as "sold_<=0" 
				 ,SUM(case when amt_loan_purchased>1 then 1 else 0 end) as "purchased_>1" 
				 ,SUM(case when amt_loan_purchased<=0 then 1 else 0 end) as "purchased_<=0"
				 ,SUM(case when (amt_loan_sold>1 or amt_loan_sold<=0 or amt_loan_purchased>1 or amt_loan_purchased<=0) then 0 else 1 end) as ok
				 ,SUM(case when plazo is null then 1 else 0 end) as "plazo_null"
				 ,COUNT(*) total
				 ,"sold_>1"*100/total::float "sold_>1_%"
				 ,"sold_<=0" *100/total::float "sold_<=0_%" 
				 ,"purchased_>1"*100/total::float "purchased_>1_%"
				 ,"purchased_<=0"*100/total::float "purchased_<=0_%"
				 ,interest_rate_null*100::float / (interest_rate_not_null+interest_rate_null)::float as "interest_rate_%"
				 ,ok*100/total::float "ok_%"
				 ,"plazo_null"*100/total::float "plazo_null_%"
				 ,amt_loan_total::float/total_sold_value::float loan_sales
				 ,amt_loan_total_kc::float/total_sold_value_kc::float loan_sales_kc
				 ,amt_loan_total::float/total_purchase_value::float loan_purchases
				 ,amt_loan_total_kc::float/total_purchase_value_kc::float loan_purchases_kc
	
	
			
			from details
			group by 1,2
			--order by 1,2 desc
			)
	select * from totals order by 1,2 desc
)  
-----------------------------Sales Lead----------------------
,sleads as (
select to_char(lead_end_date,'YYYYMM') as fk_date,country_iso,Count(*) total_sales_leads
from dwh.sales_lead sl
		INNER join dwh.dim_geography dg ON sl.fk_geography = dg.sk_geography
		
--and date(lead_end_date) = current_date -1
group by 1,2 order by 1 desc
)
-----------------------------OTHER REVENUES FROM KAVAK CAPITAL----------------------
, oth_rev as (
with car_by_transaction as (
	select ae.accounting_entry
			, MAX(scs.car_stock_id) as associated_car_id
			, MAX(scs.car_stock_name) as associated_car_name
	from stg.accounting_entry ae
	inner join serving.car_stock scs on ae.accounting_entry_item=scs.item_id 
	left join stg.account a on ae.accounting_entry_account=a.account
	where 1=1
			and ae.deleted_date is null
			and a.account_pnl_report_flag is True
	group by ae.accounting_entry
	--limit 10;
)
, po as (
	select purchase_order_id as "po_internal_id"
			, purchase_order_line_id as "po_internal_line_id"
			--, po.purchase_order_transaction_number as "transaction_number"
			--, po.purchase_order_document_number as "document_number"
			--, e_cust.entity_full_name 
			, 'Purchase Order#' || po.purchase_order_document_number || ' - ' || e_cust.entity_full_name as "transaction_number_entity"
			, po.purchase_order_status as "status"
			, e.entity_full_name as "created_by"
	from stg.bo_purchase_order po
	left join stg.entity e on po.purchase_order_created_by_id=e.entity
	left join stg.entity e_cust on po.purchase_order_client_id=e_cust.entity 
	--where purchase_order_document_number='315915'
	--limit 10;
)
select 

		--fae.accounting_entry_creation_date_timestamp as fk_date--"DATE"
		to_char(fae.accounting_entry_creation_date_timestamp,'YYYYMM') fk_date
		--,dfs.subsidiary_name country_iso
		,case when dfs.subsidiary_name like '%MEXICO%' then 'MX'
	  		  when dfs.subsidiary_name like '%ARGENTINA%' then 'AR'
	  		  when dfs.subsidiary_name like '%BRAZIL%' then 'BR'
	  	else 'Others' end country_iso
		,SUM(fae.amt_accounting_entry_total)*-1 as other_revenues_kc-- "amount"

		
from dwh.fact_accounting_entry fae  
left join dwh.dim_fiscal_subsidiary dfs on fae.fk_accounting_entry_subsidiary=dfs.sk_subsidiary
left join dwh.dim_account a on fae.fk_accounting_entry_account=a.sk_account 
where 1=1
and country_iso='MX'
		--and dfs.subsidiary_country='BR'
		and (--a.account_pnl_report_flag is true or 
		    a.account_number  
		    in ('401-010',
				'401-011',
				'401-013',
				'401-024',
				'403-001',
				'403-002'
				)
		)
		group by 1,2
)
,perfs as (

SELECT 
	to_char(summary_creation_date,'YYYYMM') as fk_date
	,summary_country_code as country_iso
 	,sum(summary_high_inician_perfilamiento) as perfs_started
 	,sum(summary_high_inician_pero_no_terminan) as perfs_incomplete
 	,sum(summary_high_terminan_perfilamiento) as perfs_complete
 	--,sum(summary_high_porcentaje_inician_perfilamiento) as summary_high_porcentaje_inician_perfilamiento
	,(sum(summary_high_inician_pero_no_terminan)::float)/(sum(summary_high_inician_perfilamiento)::float) as cr_perfs_incomplete
 	,(sum(summary_high_terminan_perfilamiento)::float)/(sum(summary_high_inician_perfilamiento)::float) as cr_perfs_complete
FROM stg.kc_kikoya_financing_funnel kkff
WHERE 1=1
and summary_country_code = 'MX'
and summary_creation_date >= '2022-01-01'
GROUP BY 1,2

)
,sims as (


select to_char(smn_creation_date,'YYYYMM') as fk_date,f.fus_country_code as country_iso
, count(distinct smn_crq_id) as sims_record
, count (distinct crq_user_id) as sims
from olimpo_postgres_global_refined.financing_db_financing_simulation sims
    
	inner join olimpo_postgres_global_refined.financing_db_financing_credit_request cr on sims.smn_crq_id = cr.crq_id
	--left join credit_request cr on cr.crq_id = s.smn_crq_id
	left join olimpo_postgres_global_refined.financing_db_financing_financial_user f on f.fus_id = cr.crq_fus_id 
	
where 1=1
  and UPPER(sims.smn_is_confirmed) = 'TRUE'
  and sims.smn_cou_id = 484
  and sims.smn_creation_date >= '2022-01-01'
  --and sims.smn_creation_date between '2022-08-15' and '2022-08-22'
  --and fk_date='202206'
  and sims.smn_simulation_origin != 'CONTACT_AUTOMATION'
  --and crq_user_id = 100911
  group by 1,2

)



-------------FINAL DATA SET----------------------


select left(d.sk_date,6) as fk_date--,d.sk_date||'01' as "date"
,h.country_iso
,h.unique_handoff,th.total_handoff
,ba.bank_approval,ba.unique_bank_approval,ba.same_conditions,ba.new_conditions,ba.unk_conditions
,b_h.denominator_bk,b_h.denominator_bk_h,b_h.numerator_15,b_h.CCR_to_date_handoff_total_bookings,b_h.CCR_15_handoff_total_bookings,b_h.CCR_15_handoff_bookings,b_h.CCR_to_date_handoff_bookings
,ba.same_conditions::float/h.unique_handoff::float as cr_handoff_same,ba.new_conditions::float/h.unique_handoff::float as cr_handoff_new,ba.unk_conditions::float/h.unique_handoff::float as cr_handoff_unk
,cr_handoff_new+cr_handoff_same+cr_handoff_unk as cr_total_handoff_bank_approvals
,ca.client_approval
,ca_bac.CCR_to_date_ca_total_ba,ca_bac.CCR_7_ca_total_ba,ca_bac.CCR_7_ca_ba
,h_ba.CCR_to_date_handoff_total_ba,h_ba.CCR_15_handoff_total_ba
,ALT.avg_b_h,ALT.avg_last_b_h,ALT.avg_h_ba,ALT.avg_ba_ca
,l.total_sold_value,l.total_purchase_value,l.total_sold_value_kc,l.total_purchase_value_kc,l.avg_loan_term,l.avg_loan_term_kc,l.rate_fee,l.rate_fee_kc,l.loan_sales,l.loan_purchases,l.amt_loan_total,l.amt_loan_total,loan_purchases_kc,loan_sales_kc
,f.total_f_sales,f.f_sales_kc_old,f.f_sales_non_kc,f.kc_sales_f_sales_old
,fn.f_sales_kc,fn.kc_sales_totals,fn.kc_sales_f_sales
,sl.total_sales_leads
,otr.other_revenues_kc
,p.perfs_started,p.perfs_incomplete,p.perfs_complete,p.cr_perfs_complete,p.cr_perfs_incomplete
,null as active_contracts
from dwh.dim_date d  --where day_is_first_of_month = 1 limit 10
	
	left join handoffs h on h.fk_date= d.sk_date
	left join t_handoffs th on th.fk_date=d.sk_date
	left join bank_approvals ba on ba.fk_date = d.sk_date
	left join CCR_bk_hnd b_h on b_h.fk_date= d.sk_date
	left join CCR_hnd_ba h_ba on h_ba.fk_date= d.sk_date
	left join client_approvals ca on ca.fk_date= d.sk_date
	left join client_approval_bac ca_bac on ca_bac.fk_date= d.sk_date
	left join ALT on ALT.fk_date= d.sk_date
	left join loan l on l.fk_date=left(d.sk_date,6)
	left join f_sales f on f.fk_date=left(d.sk_date,6)
	left join f_sales_new fn on fn.fk_date=left(d.sk_date,6)
	left join sleads sl on sl.fk_date=left(d.sk_date,6)
	left join oth_rev otr on otr.fk_date=left(d.sk_date,6)
	left join perfs as p on p.fk_date=left(d.sk_date,6)
	left join sims as s on s.fk_date=left(d.sk_date,6)

where 1=1
and day_is_first_of_month = 1
and d.year_number=2022
and d."date"<= current_date
--and sk_date>=20220000
and h.country_iso='MX' and th.country_iso='MX' and ba.country_iso='MX' and b_h.country_iso='MX' 
and h_ba.country_iso='MX' and ca.country_iso='MX' and ca_bac.country_iso='MX'
and ALT.country_iso='MX' and l.country_iso='MX' and f.country_iso='MX' and fn.country_iso='MX'
and sl.country_iso='MX'and otr.country_iso='MX' and p.country_iso='MX' and s.country_iso='MX'
order by 1--,2 desc
