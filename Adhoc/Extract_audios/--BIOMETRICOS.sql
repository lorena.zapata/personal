--BIOMETRICOS
with staff as (
				select
                *
                from playground.dl_piloto_biometricos_staff_plan_bbdd st
),
biometrics as (
				select
                *
                from playground.dl_piloto_biometricos_biometrics_data bio
)
select
DATE("date")
,st.rol
,st."location"
,st.division
,st.wd_id
,st.legacy_id
,st.work_time
,split_part(st.work_time,'-',1) as plan_entry_hour
,split_part(st.work_time,'-',2) as plan_out_hour
,split_part(bio.first_in,' ',2) as real_rentry_hour
,split_part(bio.last_in,' ',2) as real_out_hour
,split_part(bio.work_time ,' ',3) as work_time
,case when bio.first_in is not null then 1 else 0 end as attendance
,case when st.work_time = 'D' and attendance > 0 then 'attended_on_day_off'
      when st.work_time = 'HO' then 'home_office'
      when st.work_time = 'V' then 'vacations'
      when st.work_time = 'P' then 'work-licence'
      when st.work_time = 'I' then 'medical_leave'
      when char_length(st.work_time) > 2 and attendance = 0 then 'did-not-attend'
      when char_length(st.work_time) > 2 and (bio.first_in not in ('',null)) then 'attended' else 'not_planned' end as on_time
--
,case when char_length(st.work_time) > 2
          and on_time in ('attended')
          and coalesce(plan_entry_hour||':00',null) + interval '15 minutes' >= real_rentry_hour
      then 'on-time'
      when char_length(st.work_time) > 2
          and on_time in ('attended')
          and coalesce(plan_entry_hour||':00',null) + interval '15 minutes' < real_rentry_hour
      then 'late'
      /*when char_length(st.work_time) > 2
          and on_time in ('attended')
          and (to_timestamp(DATE("date")||' '||coalesce(plan_entry_hour||':00',null),'YYYY-MM-DD HH24:mi:SS') + interval '15 minutes') < to_timestamp(coalesce(to_date(bio.to_date,'yyyy-mm-dd'))||' '||split_part(case when bio.first_in in (null,'') then null else bio.first_in end,' ',2),'YYYY-MM-DD HH:mi:SS')
      then 'late'   */
 end as arrival_category
 --
,(to_timestamp(coalesce(to_date(bio.to_date,'yyyy-mm-dd'))||' '||split_part(case when bio.last_in in (null,'') then null else bio.last_in end,' ',2),'YYYY-MM-DD HH24:mi:SS')
            - to_timestamp(coalesce(to_date(bio.to_date,'yyyy-mm-dd'))||' '||split_part(case when bio.first_in in (null,'') then null else bio.first_in end,' ',2),'YYYY-MM-DD HH24:mi:SS'))
              - '09:00:00' >= '01:00:00' as extra_time
,case when
         case when split_part(split_part( coalesce(bio.work_time,'00:00:00') ,' ',3),':',1) in (null,'') then 0
              else cast(split_part(split_part( coalesce(bio.work_time,'00:00:00') ,' ',3),':',1) as int) end > 9
              then 'TIEMPO EXTRA' end as validacion_tiempo_extra/*,
,
,
,
,*/
from staff st
left join biometrics bio on (bio.wo_id||to_date(bio.to_date,'YYYY-MM-DD')  = cast(case when wd_id is null or wd_id = '' then 0 else wd_id end as int)||date(st."date")) or
                            (bio.pers_person_pin||to_date(bio.to_date,'YYYY-MM-DD')  = cast(case when st.legacy_id is null or st.legacy_id = '' then 0 else st.legacy_id end as int)||date(st."date"))
where DATE("date") between '2023-04-01' and current_date-1
order by 1 desc;