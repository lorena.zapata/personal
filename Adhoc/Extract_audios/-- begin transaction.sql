-- begin transaction
begin transaction;

-- create prev_dim_client_temp_table
CREATE TEMP TABLE prev_dim_client_temp AS (
  WITH delta_d AS (
    SELECT * 
    FROM serving.client
    WHERE update_date BETWEEN '{START_DATE}' AND '{END_DATE}'
  ),
  complemento AS (
    SELECT * 
    FROM serving.client
    WHERE update_date BETWEEN '{START_DATE}' AND '{END_DATE}'
  ),
  union_delta_w_complemento AS (
    SELECT d.* 
    FROM delta_d d
    UNION
    SELECT c.* 
    FROM complemento c 
    JOIN delta_d d ON c.client_idr = d.client_idr
  ),
  representante AS (
    SELECT *,
      CASE WHEN source_id = 1 THEN concat('n_', client_id) ELSE concat('s_', client_id) END AS client_bkr
    FROM (
      SELECT 
        ROW_NUMBER() OVER (PARTITION BY client_idr ORDER BY last_modified_date DESC, client_id DESC) AS index_row,
        *
      FROM serving.client
    ) u 
    WHERE index_row = 1 
  )
  SELECT 
    kc.client_id AS bk_client,
    left(rep.full_name, 100) AS full_name,
    rep.email,
    rep.phone,
    rep.birth_date,
    rep.gender,
    rep.city,
    rep.country,
    rep.postal_code,
    rep.fiscal_id_number1 AS fiscal_identification_number1,
    rep.fiscal_id_number2 AS fiscal_identification_number2,
    rep.document_number,
    rep.document_type,
    kc.test_flag AS is_test,
    TRUE AS is_active,
    kc.olimpo_helper_id AS bk_client_olimpo,
    CASE WHEN ROW_NUMBER() OVER (PARTITION BY kc.client_idr ORDER BY kc.last_modified_date DESC, kc.client_id DESC) = 1 THEN TRUE ELSE FALSE END AS is_representative,
    kc.client_idr AS client_idr,
    rep.client_bkr,
    kc.source_nam,
    kc.md5_dwh
  FROM union_delta_w_complemento kc 
  LEFT JOIN representante rep ON kc.client_idr = rep.client_idr
  WHERE kc.client_idr IS NOT NULL
);

-- create upserts_temp_table
CREATE TEMP TABLE dim_temp_ups_dc AS (
  SELECT tbl_src.* 
  FROM prev_dim_client_temp tbl_src 
  JOIN dwh.dim_client tbl_tgt ON tbl_src.bk_client = tbl_tgt.bk_client 
  WHERE tbl_src.md5_dwh <> tbl_tgt.md5_dwh
    AND tbl_tgt.is_active = TRUE
);

-- set inactive dwh records
UPDATE dwh.dim_client
SET 
  update_date = GETDATE(),
  end_date = GETDATE(),
  is_active = FALSE
FROM dim_temp_ups_dc 
WHERE dim_temp_ups_dc.bk_client = dwh.dim_client.bk_client 
  AND dim_temp_ups_dc.md5_dwh <> dwh.dim_client.md5_dwh
  AND dwh.dim_client.is_active = TRUE;
-- insert
INSERT INTO dwh.dim_client
            (
                bk_client,
                full_name,
                email,
                phone,
                birth_date,
                gender,
                city,
                country,
                postal_code,
                fiscal_identification_number1,
                fiscal_identification_number2,
                document_number,
                document_type,
                is_test,
                start_date,
                end_date,
                is_active,
                creation_date,
                update_date,
                md5_dwh,
                bk_client_olimpo,
                client_idr,
                is_representative,
                client_bkr,
                source_nam
            )
            (SELECT 
                bk_client,
                left(full_name,100) as full_name,
                email,
                phone,
                birth_date,
                gender,
                city,
                country,
                postal_code,
                fiscal_identification_number1,
                fiscal_identification_number2,
                document_number,
                document_type,
                is_test,
                getdate(),
                null,
                True,
                getdate(),
                getdate(),
                md5_dwh,
                bk_client_olimpo,
                client_idr,
                is_representative,
                client_bkr,
                source_nam
            FROM dim_temp_ups_dc)
        ;

INSERT INTO dwh.dim_client
           (
                bk_client,
                full_name,
                email,
                phone,
                birth_date,
                gender,
                city,
                country,
                postal_code,
                fiscal_identification_number1,
                fiscal_identification_number2,
                document_number,
                document_type,
                is_test,
                start_date,
                end_date,
                is_active,
                creation_date,
                update_date,
                md5_dwh,
                bk_client_olimpo,
                client_idr,
                is_representative,
                client_bkr,
                source_nam
            )
            (SELECT 
                tbl_src.bk_client,
                left(tbl_src.full_name,100) as full_name,
                tbl_src.email,
                tbl_src.phone,
                tbl_src.birth_date,
                tbl_src.gender,
                tbl_src.city,
                tbl_src.country,
                tbl_src.postal_code,
                tbl_src.fiscal_identification_number1,
                tbl_src.fiscal_identification_number2,
                tbl_src.document_number,
                tbl_src.document_type,
                tbl_src.is_test,
                getdate(),
                null,
                True,
                getdate(),
                getdate(),
                tbl_src.md5_dwh,
                tbl_src.bk_client_olimpo,
                tbl_src.client_idr,
                tbl_src.is_representative,
                tbl_src.client_bkr,
                tbl_src.source_nam
            FROM prev_dim_client_temp tbl_src
            LEFT JOIN dwh.dim_client tbl_tgt
              ON tbl_src.bk_client = tbl_tgt.bk_client
            WHERE tbl_tgt.bk_client IS NULL)
        ;

DROP TABLE IF EXISTS prev_dim_client_temp;
DROP TABLE IF EXISTS dim_temp_ups_dc;

END TRANSACTION;