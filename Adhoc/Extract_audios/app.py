import requests
from bs4 import BeautifulSoup
import webbrowser
import re
import flask
from flask_cors import CORS

#import pip

#def install(package):
#    pip.main(['install', package])

# Lista de librerías que necesitas
##packages = ['requests', 'BeautifulSoup', 'webbrowser','re','flask']

# Instala cada librería si no está instalada
#for package in packages:
 #   try:
  #      __import__(package)
  #  except ImportError:
  #      install(package)

##flask settings

app = flask.Flask(__name__)
CORS(app)

@app.route("/")
def main():
    # URL de la página web que deseas analizar
    url = "https://www.twr360.org/ministry/23/momento-decisivo/lang,2/?lang=2&gclid=CjwKCAjwue6hBhBVEiwA9YTx8BC9e6cIDE2hDrVxv6xFj8Lw9JIOIx5G6wuMgaN0_HKfsmloVTUakxoClBcQAvD_BwE"

    # Realizar una solicitud GET a la URL y obtener la respuesta
    respuesta = requests.get(url)

    # Analizar el contenido HTML de la página web usando BeautifulSoup
    soup = BeautifulSoup(respuesta.content, "html.parser")

    # Buscar la sección con la clase "section"
    section = soup.find(class_='latest_program')

    # Se define el nombre del audio
    audio_title = section.find('a').text
    # Buscar la clase "clase-2" dentro de la sección "section"
    clase_2 = section.find(class_='description')

    link = (section.find('a'))

    url_link = 'https://www.twr360.org' +link['href']
    #print(url_link)

    # Realizar la solicitud GET a la página web
    response = requests.get(url_link)

    # Crear objeto BeautifulSoup para analizar el contenido HTML
    soup_audio = BeautifulSoup(response.content, 'html.parser')


    audio_tag = soup_audio.find('audio', {'id': 'audio_player_html5_api'})
    #audio_src = audio_tag['src']

    soup_audio_str=str(soup_audio)

    patron = r"src:\s*'([^']*)'"
    match = re.search(patron, soup_audio_str)

    try:
        src = match.group(1)
    except:
        print("No se encontró el valor de src")
#esta ultima parte es para la descarga local
    # dwd_audio= requests.get(src)
    # try:
    #     with open(audio_title, 'wb') as archivo:
    #         archivo.write(dwd_audio.content)
    # except:
    #     print('No se pudo descargar el archivo')
        
    # print('El archivo de audio se ha encontrado correctamente. Hace click aqui para descargarlo')
    return print(src)

##main()
   
if __name__== "__main__":
    app.run()
##!jupyter nbconvert --to python app.py